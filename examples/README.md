## Due Logic Tester Examples

### dldemo1.py

Simple demo that uses immediate port writes to walk a single bit through the first 28 bits of port A.

This will trigger sample events on the outputs. Note that not all port A bits are defined as outputs, so not every write will result in an event. Only the defined output bits will trigger a sample event. If any outputs are connected to port C inputs then these will trigger additional sample events on the inputs.

Buffered samples are then listed to standard output

The first few samples after hard reset will reflect the initial port values during firmware initialisation.


### dldemo2.py

Uses objects from vcdhelper to handle the mapping between port pins and signal names.

Writes outputs to the logic tester, then reads back samples for writing to a vcd file.

The vcd file can be viewed in a waveform viewer such as GTKWave. The waveforms cover a few milliseconds.

If some of the active outputs are connected to inputs it will be visible in the waveform viewer.


### dldemo3.py

Compiles and runs a simple program in the logic tester, then reads back samples for writing to a vcd file. I/O can be driven much faster this way compared to immediate writes via USB.

The compiled program is shown on standard output.

The vcd file can be viewed in a waveform viewer such as GTKWave. The waveforms cover approximately one hundred microseconds.

If some of the active outputs are connected to inputs it will be visible in the waveform viewer.


### dldemo4.py

Exactly the same as dldemo3.py, but with timestamp transmission disabled. The vcd file now reflects the times when the samples are processed at the PC end of the connection. This can be useful for evaluating USB latency. In this case, it can be seen that a few milliseconds elapse before the samples start to arrive at the PC.

