#!/usr/bin/python3

"""Arduino Due Logic Tester demo script

Simple demo that uses immediate port writes to walk a single bit through the first 28 bits of port A.

This will trigger sample events on the outputs. Note that not all port A bits are defined as outputs, so not every write will result in an event. Only the defined output bits will trigger a sample event. If any outputs are connected to port C inputs then these will trigger additional sample events on the inputs.

Buffered samples are then listed to standard output

The first few samples after hard reset will reflect the initial port values during firmware initialisation.
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import * 


def main():

    print('Due Logic Tester Demo')

    # open a connection to the logic tester using the default comms port defined in DLSerial
    with DLSerial() as dl:

        # perform comms synchronisation with logic tester
        # this will consume comms left over from previous session
        dl.get_sync()
        
        # enable timestamp transmission
        # if disabled, timestamps will be generated from time of receipt
        # (useful for examining comms latency)
        dl.enable_timestamp(True)
        
        # set time stamp to zero
        dl.reset_timestamp()

        # ensure all outputs are off
        dl.write_outp(0)

        # walk a 1 through bits 0-27
        for i in range(28):
            dl.write_outp(1<<i)

        # finally ensure all outputs are off
        dl.write_outp(0)
        
        # print a heading for the samples
        print('{:>10}  id   {:32}'.format('time/ms', 'port value'))

        # retrieve and print the samples stored in the logic tester
        for tstamp, id, ioval in dl.get_samples():
            print('{:10.3f}  {}  {:032b}'.format(tstamp*1e-6, ('in ', 'out')[id], ioval))



if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
