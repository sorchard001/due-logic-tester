#!/usr/bin/python3

"""Arduino Due Logic Tester demo script

Using objects from vcdhelper to handle the mapping between port pins and signal names.

Writes outputs to the logic tester, then reads back samples for writing to a vcd file.

The vcd file can be viewed in a waveform viewer such as GTKWave.

If some of the active outputs are connected to inputs it will be visible in the waveform viewer.
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import * 
from vcdhelper import *


def main():

    print('Due Logic Tester Demo')


    # output port A mapping
    outputmap = PinMap(
        addr = [0,1,2,3],       # 4 bit vector, lsb first
        clk = [10]              # single bit signal
    )

    # input port C mapping
    inputmap = PinMap(
        # one big vector of all available inputs
        z = [1,2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19,21,22,23,24,25,28,29]
    )
    
    # open a new vcd file object and add the pin mappings
    vcd = VCDSignalWriter('dldemo2.vcd', timescale='1 ns')
    vcd.addpinmap(outputmap, category='demo2 outputs')
    vcd.addpinmap(inputmap, category='demo2 inputs')
 

    with DLSerial() as dl:

        # create empty dictionary to hold output signals
        opvals = {}
        
        dl.get_sync()
        dl.enable_timestamp(True)
        dl.reset_timestamp()
        dl.write_outp(0)

        # generate output signals and write them to logic tester
        for i in range(16):
            opvals['clk'] = i & 1
            opvals['addr'] = (i >> 1)
            dl.write_outp(outputmap.encode(opvals))

        dl.write_outp(0)

        # get samples from logic tester and update vcd file
        for tstamp, id, ioval in dl.get_samples():
            if id == 1:
                newvals = outputmap.decode(ioval)
            else:
                newvals = inputmap.decode(ioval)

            vcd.updatesignals(newvals, t=tstamp)



if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
