#!/usr/bin/python3

"""Arduino Due Logic Tester demo script

Compiling and running a simple program in the logic tester. I/O can be driven much faster this way compared to immediate writes via USB.

Writes outputs to the logic tester, then reads back samples for writing to a vcd file.

The vcd file can be viewed in a waveform viewer such as GTKWave.

If some of the active outputs are connected to inputs it will be visible in the waveform viewer.
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import * 
from vcdhelper import *
from dlcompile import *


def main():

    print('Due Logic Tester Demo')


    # define output locations as 'constants'
    OUTPUT_A = 0
    OUTPUT_B = 1
    
    # output port A mapping
    outputmap = PinMap(
        # two single bit signals
        a = [OUTPUT_A],
        b = [OUTPUT_B]
    )

    # input port C mapping
    inputmap = PinMap(
        # one big vector of all available inputs
        z = [1,2,3,4,5,6,7,8,9,12,13,14,15,16,17,18,19,21,22,23,24,25,28,29]
    )
    
    # open a new vcd file object and add the pin mappings
    vcd = VCDSignalWriter('dldemo3.vcd', timescale='1 ns')
    vcd.addpinmap(outputmap, category='demo3 outputs')
    vcd.addpinmap(inputmap, category='demo3 inputs')
 


    with DLSerial() as dl:

        dl.get_sync()
        dl.enable_timestamp(True)
        dl.reset_timestamp()

        # define some symbols
        my_symbols = {
            'output_a': OUTPUT_A,
            'output_b': OUTPUT_B,
            'num_loops': 16
        }

        # define program
        prog = [
            'org 0',
            'trst',
            'for num_loops',
            'togbpi p0 output_a',
            'togbpi p0 output_b',
            'next',
            'end'
        ]
        
        # compile program
        objcode = dlcompile(prog, symbols=my_symbols, showcode=True)
        
        # write program to logic tester
        dl.write_prog(objcode, 0)

        # run program
        dl.run_prog(0)

        # get samples from logic tester and update vcd file
        for tstamp, id, ioval in dl.get_samples():
            if id == 1:
                newvals = outputmap.decode(ioval)
            else:
                newvals = inputmap.decode(ioval)

            vcd.updatesignals(newvals, t=tstamp)



if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except DLCompError as e:
        print('Due Logic Compilation Error:\n{}'.format(e))

    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
