## Due Logic Tester Python Documentation

### dlserial.py

Provides the `DLSerial` object that implements the interface with the logic tester. It is derived from the `pyserial.Serial` object.

A default serial port is defined near the top of `dlserial.py`. This can be edited to avoid having to specify the port when creating a `DLSerial` object:

```
# Define default serial port (e.g. Linux)
DEFAULTSERPORT = '/dev/ttyACM0'

# Define default serial port (e.g. Windows)
DEFAULTSERPORT = 'COM30'
```

#### `class dlserial.DLSerial(serial.Serial)`
> __`__init__(serport=DEFAULTSERPORT)`__
>
> Create and open a connection to the tester. If the `serport` parameter is not specified, then `dlserial.DEFAULTSERPORT` is used as a default.
>
> __`get_sync()`__
>
> Synchronises communication with the tester. This works by sending a sync command with a pseudorandom payload to the tester and waiting for the same payload to be echoed back. All other communication from the tester is discarded until the correct echo is received.
>
> __`reset_timestamp()`__
> 
> Reset the timestamp in the tester to zero. Also resets the local timestamp.
>
> __`enable_timestamp(enable)`__
> 
> Enables or disables timestamp transmission from the tester. Disabling timestamps increases the transmission rate from the tester. When timestamps are disabled, the python code applies a local timestamp to each sample as it is read out of the local buffer.
> 
> The `enable` parameter is either `True` or `False`
>
> __`write_outp(opval)`__
> 
> Immediately sets the state of all outputs.
> * `opval` is a 32-bit integer representing the output state.
>
> __`end_of_samples()`__
> 
> Requests an end of sample marker. This allows the `get_samples()` function to complete early instead of timing out.
>
> __`write_prog(program, startaddr=0)`__
>
> Uploads a virtual machine language program to a specified address in the logic tester.
> * `program` is an iterable type containing 8-bit integers.
> * `startaddr` is an integer in the range 0 - 16383.
>
> __`run_prog(startaddr=0)`__
>
> Runs a program in the logic tester starting at the specified address.
> * `startaddr` is an integer in the range 0 - 16383.
>
> __`get_samples()`__
>
> Generator that yields samples received from the logic tester. For each sample a tuple is returned that consists of `tstamp, id, ioval`
> * `tstamp` is an integer value representing nanoseconds.
> * `id` is an integer identifying which i/o port the sample belongs to. `0` for input port C, and `1` for output port A.
> * `ioval` is a 32-bit integer representing the port state.
---
### vcdhelper.py

Provides objects to create and write to value change dump (vcd) files, handle mappings between signal names and physical I/O and the detection of signal events.

#### `class vcdhelper.PinMap(dict)`
>This is a dictionary class that associates signal names with physical i/o lines. Functions are provided to convert between signal values and port values.
> __`__init__(*args, **kwargs)`__
>
> Creates a dictionary of mappings. For example:
> ```
> outputmap = vcdhelper.PinMap(
>     sig1 = [0],
>     sig2 = [1,2,3,4]
> )
> ```
>
> `sig1` is specified as one i/o line, while `sig2` is a four bit vector (specified least significant bit first). i/o lines are specified by their bit position within the port.
>
> __`decode(wordval)`__
>
> Converts a port value into a dictionary of signals & associated values.
> * `wordval` is a 32-bit integer representing the port state.
> * returns a dictionary of signals and their associated values.
>
> __`encode(vals)`__
>
> Converts a dictionary of signals & associated values into a port state.
> * `vals` is a dictionary of signals and their associated values.
> * returns a 32-bit integer representing the port state.
>
> Note that if a port bit is referenced multiple times then its final value will be a logical OR of the contributing bits.
>
#### `class vcdhelper.Signal`
> This is a simple class that represents a signal state.
>
> __`curval`__
>
> The current value of the signal
>
> __`oldval`__
>
> The previous value of the signal
>
> __`category`__
>
> This is a string that identifies the signal category. This is used by the `VCDSignalWriter` class to group signals together in a waveform viewer.
>
> __`__init__(**kwargs)`__
> 
> Create a signal. Can take a keyword argument to set the category. For example:
> `sig1 = vcdhelper.Signal(category='outputs')`
> 
> __`setval(newval, **kwargs)`__
>
> Sets the signal state to `newval`
>
> __`rising()`__
>
> Returns `True` if the current state is greater than the previous state. This has the side effect of updating the previous state, such that a subsequent read will return `False`.
>
> __`falling()`__
>
> Returns `True` if the current state is less than the previous state. This has the side effect of updating the previous state, such that a subsequent read will return `False`.
>
#### `class vcdhelper.SignalDict(dict)`
> Maintains a dictionary of names and associated signals.
>
> __`addsignal(name, **kwargs)`__
>
> Creates and adds a new signal to the dictionary.
> * `name` is a string containing the signal name.
> * Accepts the keyword `category` to specify the signal category.
>
> __`addpinmap(pinmap, **kwargs)`__
>
> Creates and adds new signals with the names found in the specified pinmap.
> * `pinmap` is a `vcdhelper.PinMap`
> * Accepts the keyword `category` to specify the category of added signals.
>
> __`updatesignal(name, newval, **kwargs)`__
>
> Updates the value of a signal in the dictionary.
> * `name` is a string specifying the signal name.
> * `newval` is the new value to set.
>
> __`updatesignals(newvals, **kwargs)`__
>
> Updates the values of several signals in the dictionary.
> * `newvals` is a dictionary of signal names and associated values, for example the output of the `vcdhelper.PinMap.decode()` function.

#### `class vcdhelper.VCDSignalWriter(VCDWriter, SignalDict)`
> This class builds on `vcdhelper.SignalDict` by adding value change dump (vcd) file output.
>
> __`__init__(filename, **kwargs)`__
>
> * `filename` is a string containing the name of the output vcd file
>
> Can take the keyword argument `timescale` to specify the time resolution to the waveform viewer. For example:
> `vcd = vcdhelper.VCDSignalWriter('file.vcd', timescale='1 ns')`
>
> __`close(*args, **kwargs)`__
>
> Close the vcd file.
>
> __`addsignal(name, **kwargs)`__
>
> Creates and adds a new signal to the dictionary.
> * `name` is a string containing the signal name.
> * Accepts the keyword `category` to specify the signal category.
>
> __`addpinmap(pinmap, **kwargs)`__
>
> Creates and adds new signals with the names found in the specified pinmap.
> * `pinmap` is a `vcdhelper.PinMap`
> * Accepts the keyword `category` to specify the category of added signals.
>
> __`updatesignal(name, newval, **kwargs)`__
>
> Updates the value of a signal in the dictionary.
> * `name` is a string specifying the signal name.
> * `newval` is the new value to set.
>
> __`updatesignals(newvals, **kwargs)`__
>
> Updates the values of several signals in the dictionary.
> * `newvals` is a dictionary of signal names and associated values, for example the output of the `vcdhelper.PinMap.decode()` function.
---
### dlcompile.py

Provides a function that assembles a program into machine code suitable for uploading to the logic tester.

#### `dlcompile.dlcompile(lines, **kwargs)`
> * `lines` is an iterable of strings containing the lines of the program to be assembled.
> * Accepts the keyword argument `showcode=True` to show the assembled program on standard output.
> * Accepts the keyword argument `symbols` to specify a dictionary of symbols and associated values.
> * Returns a list of 8-bit integers containing the assembled program in a form suitable for uploading to the tester.
> * The symbols may be accessed after assembly via the global dictionary `_symbols`
>
> Refer to the document `assembly.md` for the assembly language syntax.
