## Due Logic Tester Assembly Language

The program is presented to the `dlcompile` function as an iterable sequence of strings (e.g. a list or file). This function returns a sequence of bytes that can then be uploaded and run in the tester. For example:

```
    # define program
    prog = [
        '; Toggle port A bit 0 eight times',
        'org 0',
        'trst',
        'for 8',
        'togbpi p0 0',
        'next',
        'end'
    ]
        
    # compile program
    objcode = dlcompile(prog, showcode=True)
    
    # write program to logic tester
    # (dl is a previously opened instance of DLSerial)
    dl.write_prog(objcode, 0)

    # run program
    dl.run_prog(0)
```

Note that a comment line begins with a semicolon `;`. Comments within the code are probably more useful if the source is held in a file, otherwise Python comments would suffice.

### Symbols

Symbols may be used in place of numeric values or addresses. Symbols may be defined in a dictionary and passed to `dlcompile`.

Symbols may also be defined as address labels for branches, jumps and data tables. An address label is recognised if the first token on a line ends in a colon `:` Note that the colon does not form part of the symbol name.

The program is assembled in two passes to allow forward code references.

Example:

```
    mysymbols = {
        'start_addr' : 1000,
        'num_loops' : 16,
        'output_bit' : 0
    }

    prog = [
        'org start_addr',
        'trst',
        'for num_loops',
        'bsr togbit',
        'next',
        'end',
        'togbit:',
        'togbpi p0 output_bit',
        'rts'
    ]
        
    objcode = dlcompile(prog, symbols=mysymbols, showcode=True)
```

There is no support for expressions within the assembly language program though expressions can of course be used at the Python level.

### Table of Instructions

| Mnemonic | Operands   | Description                               |
|:---------|:-----------|:------------------------------------------|
| nop      |            | No operation                              |
| trst     |            | Reset timestamp                           |
| flush    |            | Flush sample buffer                       |
| smpoff   |            | Disable sampling                          |
| smpon    |            | Enable sampling                           |
| smpa     |            | Sample port A (outputs)                   |
| smpc     |            | Sample port C (inputs)                    |
| for      | imm16      | Loop code specified number of times between `for` and `next` |
| next     |            | Marks the end of the `for/next` loop      |
| end      |            | End program, flush sample buffer and return tester to interactive mode |
|          |            |                                           |
| ldrr     | reg reg    | Load first register from second register  |
| ldrp     | reg port   | Load register from port                   |
| ldri     | reg imm32  | Load register with immediate value        |
| ldpr     | port reg   | Load port from register                   |
| ldpi     | port imm32 | Load port from immediate value            |
| ldrx     | reg reg    | Load register from address in register with auto increment |
|          |            |                                           |
| incr     | reg        | Increment register                        |
| decr     | reg        | Decrement register                        |
|          |            |                                           |
| orri     | reg imm32  | Logical OR register with immediate value  |
| orrr     | reg reg    | Logical OR register with register         |
| andri    | reg imm32  | Logical AND register with immediate value |
| andrr    | reg reg    | Logical AND register with register        |
| eorri    | reg imm32  | Logical EOR register with immediate value |
| eorrr    | reg reg    | Logical EOR register with register        |
|          |            |                                           |
| setbri   | reg imm8   | Set register bit 0-31                     |
| setbpi   | port imm8  | Set port bit 0-31                         |
| clrbri   | reg imm8   | Clear register bit 0-31                   |
| clrbpi   | port imm8  | Clear port bit 0-31                       |
| togbri   | reg imm8   | Toggle register bit 0-31                  |
| togbpi   | port imm8  | Toggle port bit 0-31                      |
| tstbri   | reg imm8   | Test register bit 0-31                    |
| tstbpi   | port imm8  | Test port bit 0-31                        |
|          |            |                                           |
| bra      | off8       | Relative branch                           |
| beq      | off8       | Relative branch on zero                   |
| bne      | off8       | Relative branch on not zero               |
| bsr      | off8       | Relative branch to subroutine             |
| jmp      | addr16     | Absolute jump                             |
| jeq      | addr16     | Absolute jump on zero                     |
| jne      | addr16     | Absolute jump on not zero                 |
| jsr      | addr16     | Absolute jump to subroutine               |
| rts      |            | Return from subroutine                    |
|          |            |                                           |
| org      | addr16     | (Directive) Set program origin            |
| ddw      | imm32      | (Directive) Define 32-bit data            |

#### Instruction Notes

* A program should always terminate with the `end` instruction to flush the sample buffer and return the tester to immediate mode.
* `for/next` loops can be nested 8 levels deep.
* Subroutines can be nested 8 levels deep.
* The `ldrx` instruction exists to make it easier to write a series of values to a port. A table can be set up in memory using `ddw` directives and this table can then be accessed sequentially with `ldrx`.
* If a program generates too many samples before it ends then the sample buffer will overflow unless explicit `flush` commands are given.

#### Operand Types

| Operand | Description                      |
|:--------|:---------------------------------|
| addr16  | Program address in range 0-16383 |
| imm8    | Immediate 8-bit value            |
| imm16   | Immediate 16-bit value           |
| imm32   | Immediate 32-bit value           |
| off8    | Relative signed 8-bit branch     |
| port    | Any port p0, p1, p2, p3          |
| reg     | Any register r0 - r255           |

##### Operand Notes:

* Registers and ports are 32-bit.
* p0, p1, p2, p3 correspond to ports A, B, C, D respectively, but only p0 (outputs) and p2 (inputs) are currently supported.


