#!/usr/bin/python3

"""74LS783 SAM

Explores video mode changes during active video

Walks a mode change one cycle at a time through a number of scanlines
Repeats for all 24 possible mode changes
Writes output to a zip file as it generates a lot of data

Captured data very close to actual hardware but fails to capture
metastability and race conditions found in Dragon machines
"""

import os
import sys
sys.path.append('..')   # add parent folder to path for imports
import zipfile

from dlserial import *
from sam import DLSAM

    

def main():

    # build a list of all possible video mode changes
    vmodes = ['y12', 'x3', 'y3', 'x2', 'y2', 'x1', 'y1', 'dma']
    modelist = ((v, b) for v in range(8) for b in range(3))

    # number of scanlines to run. O(n*n) so gets very slow
    # (twice as many lines takes four times longer)
    numlines = 4

    # d64 option: hsync timing is slightly different
    d64 = False

    # option to write vcd files to vcd subfolder
    # debug only: each mode run overwrites previous
    makevcd = False


    with DLSAM() as dl:

        if makevcd:
            os.makedirs('vcd', exist_ok=True)

        dl.enable_timestamp(False)

        dl.resetsam()
        
        zipfilename = 'samcap-{}-{}.zip'.format(('d32','d64')[d64], numlines)
        print(zipfilename)
        with zipfile.ZipFile(zipfilename, 'a', compression=zipfile.ZIP_DEFLATED) as zip:

            for initvmode, togvmode in modelist:

                newvmode = initvmode ^ (1<<togvmode)

                filename = 'vid-{}-{}'.format(vmodes[initvmode], vmodes[newvmode])
                print(filename)
                
                with zip.open(filename, 'w') as f:
                    
                    dl.preparemodechangecode(initvmode, togvmode, makevcd, f, d64=d64, faketime=True)
                    
                    for i in range(57*numlines):
                        dl.runmodechangecode(numlines, i, faketime=True)
                    

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
