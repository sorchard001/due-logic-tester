#!/usr/bin/python3

"""74LS783 SAM

Illustrates transparent latch behaviour in the control register

SAM is clocked until E is high and the mpu column address is at the outputs
Then addresses are supplied to toggle memory config and page bits (without further clocking)
These changes appear immediately in the column address

This would not happen if the control bits were registered
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(True)
    
        # get sam into known state
        dl.resetsam()
        
        # set memory config to 64K
        dl.mpucycles(['w 0xffdd'])

        # get the current output values
        opvals = {op: dl.vcd[op].curval for op in dl.outputmap}

        dl.reset_timestamp()

        # clock SAM until mpu column address appears (with E high)
        for i in range(11):
            for osc in (1,0):
                opvals['osc'] = osc
                dl.write_outp(dl.outputmap.encode(opvals))
  
        # output addresses to toggle some control bits
        # these will show up in the column address if the control bits are latches
        # (as opposed to registers that update on a clock edge)
        opvals['rw'] = 0
        for addr in (0xffdc, 0xffdd, 0xffd5, 0xffd4, 0xffdf, 0xffde, 0xffff):
            opvals['addr'] = addr
            dl.write_outp(dl.outputmap.encode(opvals))

        dl.end_of_samples()
    
        dl.handle_samples(vcdfilename='sam-ctl-reg.vcd')
        
        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
