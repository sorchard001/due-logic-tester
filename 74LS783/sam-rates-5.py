#!/usr/bin/python3

"""74LS783 SAM

Explores when the sam decides to run a fast or slow cycle in AD mode

Walks address transitions through the sam cycle
Generates several vcd files in a subfolder

Waveforms suggest:
- slow-fast transition detected at t3
- fast-slow transition detected at t0 & t8
"""

import os
import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM



def main():

    with DLSAM() as dl:
    
        outputdir = 'sam-rates-5'
        os.makedirs(outputdir, exist_ok=True)

        dl.enable_timestamp(False)
    
        # get sam into known state
        dl.resetsam(sampenable=True)

        # set AD rate then run a slow bus cycle
        dl.mpucycles(['w 0xffd7', 'r 0x0000'])

        # get the current output values
        opvals = {op: dl.vcd[op].curval for op in dl.outputmap}

        # function that toggles sam clock input specified number of times
        def clocksam(youdonthavetoputontheredlight):
            for i in range(youdonthavetoputontheredlight):
                opvals['osc'] = 1 - opvals['osc']
                dl.write_outp(dl.outputmap.encode(opvals))

        dl.reset_timestamp()

        # move address transition through a range of times
        for addrpos in range(32):

            # clock sam until we reach desired point
            clocksam(addrpos)
          
            # output fast address
            opvals['addr'] = 0xffff
            dl.write_outp(dl.outputmap.encode(opvals))

            # complete the sam cycle plus add another cycle
            clocksam(64-addrpos)

            # clock sam until we reach desired point
            clocksam(addrpos)
          
            # output slow address
            opvals['addr'] = 0
            dl.write_outp(dl.outputmap.encode(opvals))

            # complete the sam cycle plus add another cycle
            clocksam(64-addrpos)

            dl.end_of_samples()
            filename = '{}/sam-ad-timing-{}.vcd'.format(outputdir, addrpos)
            dl.handle_samples(vcdfilename=filename, keep_tref=True, faketime=True)

        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
