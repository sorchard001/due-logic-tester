#!/usr/bin/python3

"""74LS783 SAM demo

Simulates a Dragon 32 video frame and writes refresh and video addresses to the text file 'sam-demo3.txt'
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import * 
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(True)
    
        dl.resetsam()

        # set mem type to 64K dynamic
        addrlist = [
            'w 0xffdd'
        ]
        dl.mpucycles(addrlist)
                
        # vsync function resets video address counter in SAM.
        # It does this by raising hs while da0 high
        # This also triggers a refresh sequence so the function
        # runs enough oscillator cycles to consume the refresh.
        # No samples are returned by default, so we manually enable samples
        # to allow decoder to acquire SAM phase.
        dl.vsync(sampenable=True)
        
        # run for active part of video frame
        # keep_tref=True informs decoder to not bother acquiring SAM phase
        # (already established)
        # sampmode='video' reduces the sampling to the minimum necessary
        #
        with open('sam-demo3.txt', 'w') as f:
            for i in range(192):
                print('scanline {}'.format(i))
                dl.scanline(keep_tref=True, sampmode='video', file=f)
                f.write('\n')



if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
