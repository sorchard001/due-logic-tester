#!/usr/bin/python3

"""74LS783 SAM demo

Runs a series of mpu bus cycles and writes the results to a vcd file to show the SAM in action.

First resets the SAM control register bits then selects 64K dynamic memory type to make it easier to see how the addresses are multiplexed.
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import * 
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(True)
    
        # Reset SAM to put it into a known state.
        # It works by running a sequence of bus cycles that clears
        # all 16 control register bits.
        # No samples are returned by default.
        dl.resetsam()

        # run a series of mpu bus cycles
        addrlist = [
            'r 0xffff',
            'w 0xffdd',     # set mem type to 64K dynamic
            'r 0x1234',
            'w 0x5678',
            'r 0x8000',
            'r 0xa000',
            'r 0xc000',
            'r 0xff00',
            'r 0xff20',
            'r 0xff40',
            'r 0xffc0',
            'r 0xffe0',
            'r 0xffff'
        ]
        dl.mpucycles(addrlist, vcdfilename='sam-demo1.vcd', faketime=True)



if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
