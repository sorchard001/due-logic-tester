#!/usr/bin/python3

"""74LS783 SAM

Demonstration of timing violations when changing the rate setting

In all cases this appears to be the q clock falling at a slow timing point
too soon after the e clock rises at a fast timing point

The change from AD to slow must occur at the start of a SAM cycle and
must immediately follow a fast cycle at the end of the previous SAM cycle.
The SAM datasheet gives a code example that performs this synchronisation.

The change from fast to slow must occur at the start of a SAM cycle.
As there are no slow cycles in this mode there is no software method
(other than cycle counting) of achieving it directly. This is why
the SAM datasheet disallows this transition and requires an intermediate
step via AD mode.
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(False)
    
        # get sam into known state
        dl.resetsam()

        # list of test addresses
        addrlist = [
            'r 0xffff',
            'w 0xffd7',
            'r 0xffff',
            'w 0xffd6',     # ok
            'w 0xffd7',
            'w 0xffd6',     # this causes q to fall too quickly after e
            'w 0xffd7',
            'r 0x0000',
            'w 0xffd6',     # this causes q to fall too quickly after e
            'w 0xffd7',
            'r 0xffff',
            'r 0xffff',
            'w 0xffd6',     # this causes q to fall too quickly after e
            'r 0xffff',
            'w 0xffd9',
            'w 0xffd8',     # ok
            'w 0xffd9',
            'r 0xffff',
            'r 0xffff',
            'w 0xffd8',     # ok
            'w 0xffd9',
            'r 0xffff',
            'w 0xffd8',     # this causes q to fall too quickly after e
            'r 0xffff'
        ]
         
        dl.mpucycles(addrlist, vcdfilename='sam-rates-3.vcd', faketime=True)
        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
