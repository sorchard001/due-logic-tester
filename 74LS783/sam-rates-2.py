#!/usr/bin/python3

"""74LS783 SAM

mpu rate changes in address dependent mode

This explores slow vs fast addresses in both map types
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(False)
    
        # get sam into known state
        dl.resetsam()

        # list of test addresses
        addrlist = [
            'r 0x0000',
            'r 0x8000',
            'r 0xa000',
            'r 0xc000',
            'r 0xff00',
            'r 0xff20',
            'r 0xff40',
            'r 0xff60',
            'r 0xffc0',
            'r 0xffe0',
            'r 0xffff'
        ]

        # set address dependent rate 
        cyclelist = ['w 0xffd7']
        cyclelist.extend(addrlist)
        # set map type 1
        cyclelist.append('w 0xffdf')
        cyclelist.extend(addrlist)
           
        dl.mpucycles(cyclelist, vcdfilename='sam-rates-2.vcd', faketime=True)
        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
