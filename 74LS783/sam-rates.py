#!/usr/bin/python3

"""74LS783 SAM

mpu rate changes in address dependent mode

This demonstrates that at least three consecutive fast cycles are required to achieve a gain
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(False)
    
        # get sam into known state
        dl.resetsam()


        addrlist = [
            'w 0xffd7',
            'r 0x0000',
            'r 0xffff',
            'r 0x0000',
            'r 0xffff',
            'r 0xffff',
            'r 0x0000',
            'r 0xffff',
            'r 0xffff',
            'r 0xffff',
            'r 0x0000',
            'r 0xffff',
            'r 0xffff',
            'r 0xffff',
            'r 0xffff',
            'r 0x0000'
        ]
           
        dl.mpucycles(addrlist, vcdfilename='sam-rates.vcd', faketime=True)
        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
