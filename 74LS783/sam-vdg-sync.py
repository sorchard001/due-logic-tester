#!/usr/bin/python3

"""74LS783 SAM

Explores sam-vdg synchronisation by walking da0 through timing window

Generates several vcd files in a subfolder
"""

import os
import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        outputdir = 'sam-vdg-sync'
        os.makedirs(outputdir, exist_ok=True)

        dl.enable_timestamp(False)
    
        # get sam into known state
        # sampling enabled so we can get output state
        dl.resetsam(sampenable=True)

        #dl.mpucycles(['r 0xffff'])


        # get the current output values
        opvals = {op: dl.vcd[op].curval for op in dl.outputmap}

        dl.reset_timestamp()

        # move da0 transition through a range of times covering error window
        # 22 - 25 should fall within window
        for da0pos in range(15, 32):

            # run four sam cycles per position
            # this covers two da0 rising edges
            for j in range(4):

                # clock sam until we reach desired point
                for i in range(da0pos):
                    opvals['osc'] = 1 - opvals['osc']
                    dl.write_outp(dl.outputmap.encode(opvals))
          
                # toggle da0
                opvals['da0'] = 1 - opvals['da0']
                dl.write_outp(dl.outputmap.encode(opvals))

                # complete the sam cycle
                for i in range(32-da0pos):
                    opvals['osc'] = 1 - opvals['osc']
                    dl.write_outp(dl.outputmap.encode(opvals))

            dl.end_of_samples()
            filename = '{}/sam-vdg-sync-{}.vcd'.format(outputdir, da0pos)
            dl.handle_samples(vcdfilename=filename, faketime=True)

        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
