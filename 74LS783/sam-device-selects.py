#!/usr/bin/python3

"""74LS783 SAM

"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(False)
    
        # get sam into known state
        # enable sampling so we can determine i/o state
        dl.resetsam(sampenable=True)
        
        # list of test addresses
        addresses = [
            0x0000, 0x8000, 0xa000, 0xc000,
            0xff00, 0xff20, 0xff40, 0xff60, 0xffe0
        ]
        
        # build empty results table
        results = {a: [-1,-1,-1,-1] for a in addresses}
        
        for map in (0,1):
        
            print('map{}'.format(map))

            # set map type
            if map == 1:
                dl.mpucycles(['w 0xffdf'])

            # get the current output values
            opvals = {op: dl.vcd[op].curval for op in dl.outputmap}
            
            # change address and rw lines to create new samples
            for a in addresses:
                for rw in (0,1):
                    opvals['addr'] = a
                    opvals['rw'] = rw
                    dl.write_outp(dl.outputmap.encode(opvals))
                    
            # mark the end of the samples
            # not necessary but allows get_samples() to return faster
            # instead of timing out.
            dl.end_of_samples()
        
            # get new samples
            for tstamp, id, ioval in dl.get_samples():
                if id == 1:
                    newvals = dl.outputmap.decode(ioval)
                else:
                    newvals = dl.inputmap.decode(ioval)

                dl.vcd.updatesignals(newvals, t=tstamp)

                # add info to results table
                # each entry may be written multiple times
                # but it's the last update that matters
                addr = dl.vcd['addr'].curval
                rw = dl.vcd['rw'].curval
                s = dl.vcd['s'].curval
                results[addr][rw+map*2] = s
            
        with open('sam-device-selects.txt', 'w') as f:
            f.write('addr [map0_w, map0_r, map1_w, map1_r]\n\n')
            for addr, s in results.items():
                ln = '{:04X} {}'.format(addr, s)
                print(ln)
                print(ln, file=f)

                
        # now generate vcd file
        testcycles = [
            '{} 0x{:04x}'.format(rw, a)
            for a in addresses
            for rw in ('w', 'r')
        ]
            
        buscycles = ['w 0xffde']
        buscycles.extend(testcycles)
        buscycles.append('w 0xffdf')
        buscycles.extend(testcycles)
        
        dl.mpucycles(buscycles, vcdfilename='sam-device-selects.vcd', faketime=True)
        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
