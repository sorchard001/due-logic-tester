#!/usr/bin/python3

"""74LS783 SAM demo

Simulates a Dragon 32 video scanline and writes the result to a vcd file

Demonstrates decoding of refresh and video addresses
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import * 
from sam import DLSAM


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(True)
    
        dl.resetsam()

        # set mem type to 64K dynamic, video mode to 6K
        addrlist = [
            'w 0xffdd',
            'w 0xffc3',
            'w 0xffc5'
        ]
        dl.mpucycles(addrlist)
                
        # vsync function resets video address counter in SAM.
        # It does this by raising hs while da0 high
        # This also triggers a refresh sequence so the function
        # runs enough oscillator cycles to consume the refresh.
        # No samples are returned by default, so we manually enable samples
        # to allow decoder to acquire SAM phase.
        dl.vsync(sampenable=True)
        
        # run for complete video scanline
        # keep_tref=True informs decoder to not bother acquiring SAM phase
        # (already established during vsync)
        dl.scanline(keep_tref=True, vcdfilename='sam-demo2.vcd', faketime=True)



if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
