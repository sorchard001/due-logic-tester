#!/usr/bin/python3

"""74LS783 SAM

Demonstration of the recommended way to change the rate setting from AD to slow

It can be seen that this is only necessary for code running from rom
In this case the TST instruction performs the synchronisation

For code running from ram a simple sta $ffd6 is sufficient
because the slow instruction fetch performs the synchronisation
"""

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from sam import DLSAM


# returns instruction sequence to change from AD rate to slow
def slowdownseq(addr):
    return [
        # set fast rate
        'w 0xffd7',
        
        # recommended slowdown sequence

        # TST $0000
        'r 0x{:4x}'.format(addr),   # fetch
        'r 0x{:4x}'.format(addr+1), # fetch
        'r 0x{:4x}'.format(addr+2), # fetch
        'r 0xffff',                 # nvma
        'r 0x0000',                 # data
        'r 0xffff',                 # nvma
        'r 0xffff',                 # nvma

        # BRN 0
        'r 0x{:4x}'.format(addr+3), # fetch
        'r 0x{:4x}'.format(addr+4), # fetch
        'r 0xffff',                 # nvma

        # STA $FFD6
        'r 0x{:4x}'.format(addr+5), # fetch
        'r 0x{:4x}'.format(addr+6), # fetch
        'r 0x{:4x}'.format(addr+7), # fetch
        'r 0xffff',                 # nvma
        'w 0xffd6'                  # write
    ]


def main():

    with DLSAM() as dl:
    
        dl.enable_timestamp(False)
    
        # get sam into known state
        dl.resetsam()

        # rom based sequence
        addrlist = slowdownseq(0x8000)
       
        # ram based sequence
        addrlist.extend(slowdownseq(0x1000))
         
        dl.mpucycles(addrlist, vcdfilename='sam-rates-4.vcd', faketime=True)
        

if __name__ == '__main__':

    try:
        main()
        sys.exit(0)

    except DLError as e:
        print('Due Logic Tester error:\n{}'.format(e.message))
        
    except KeyboardInterrupt:
        print('Script terminated by user')
        
    except serial.SerialException as e:
        print('Serial error: {}'.format(e))

    sys.exit(1)
