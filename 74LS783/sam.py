#!/usr/bin/python3

import sys
sys.path.append('..')   # add parent folder to path for imports

from dlserial import *
from vcdhelper import *
from dlcompile import *


class SAMControlReg:
    """Representation of SAM control register"""
    
    def __init__(self):
        self.val = 0
        self.update()
        
    def setbit(self, bitnum):
        self.val |= (1 << bitnum)
        self.update()
        
    def clrbit(self, bitnum):
        self.val &= ~(1 << bitnum)
        self.update()

    def update(self):
        self.v = self.val & 7
        self.f = (self.val << 6) & 0xfe00
        self.p = (self.val >> 10) & 1
        self.r = (self.val >> 11) & 3
        self.m = (self.val >> 13) & 3
        self.t = (self.val >> 15) & 1

    def valstring(self):
        """return a string describing control register state"""
        # note vcd file does not allow spaces in string value
        return 'V={}|F={:04X}|P={}|R={}|M={}|T={}'.format(
            self.v, self.f, self.p, self.r, self.m, self.t
        )


class DLSAM(DLSerial):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        print('waiting sync')
        self.get_sync()

        # Arduino Due output port A mapping
        self.outputmap = PinMap(
            addr = [0,1,2,3,4,6,7,10,11,12,13,14,15,16,17,18],
            osc = [19],
            rw = [22],
            hs = [23],
            da0 = [24]
        )

        # Arduino Due input port C mapping
        self.inputmap = PinMap(
            z = [2,3,4,5,6,7,8,9],
            z6_0 = [2,3,4,5,6,7,8],
            ras1 = [9],
            we = [12],
            vclk = [13],
            ras0 = [14],
            cas = [15],
            e = [16],
            q = [17],
            s = [23,24,25]
        )
        
        self.samreg = SAMControlReg()
        self.tref = -1


    def runprog(self, program, **kwargs):
        """compile, send and run a program in the logic tester"""

        objcode = dlcompile(program, **kwargs)
        self.write_prog(objcode)
        self.run_prog()
        self.handle_samples(**kwargs)


    def handle_samples(self, **kwargs):
        """receive and decode samples"""
        
        self.oldtstamp = 0
        vcdfilename = kwargs.get('vcdfilename', '')
        decodevid = (kwargs.get('sampmode', '') == 'video')
        opfile = kwargs.get('file', sys.stdout)
        faketime = kwargs.get('faketime', False)

        # keep_tref only works if osc cycle continues cleanly
        if not kwargs.get('keep_tref', False):
            self.tref = -1
         
        if len(vcdfilename) == 0:
            vcd = SignalDict()
        else:
            print(vcdfilename)
            vcd = VCDSignalWriter(vcdfilename, timescale='1 ns')
        
        if True:
        
            vcd.addpinmap(self.outputmap, category='sam_inputs')
            vcd.addpinmap(self.inputmap, category='sam_outputs')
    
            vcd.addsignal('tref', category='decode', type='string', size=4)
            vcd.addsignal('samreg', category='decode', type='string', size=30)
            vcd.addsignal('vidaddr', category='decode', type='wire', size=16)
            vcd.addsignal('refresh', category='decode', type='wire', size=8)

            vcd.updatesignal('samreg', self.samreg.valstring(), t=0)

            tref = self.tref
            samplecount = 0
            vidaddr = -1
            bank = -1
            casflag = True
            rowflag = True
            t = 0
            oldoscval = 0
            
            for tstamp, id, ioval in self.get_samples():
                samplecount += 1
                if id == 1:
                    newvals = self.outputmap.decode(ioval)
                    if faketime:
                        newoscval = newvals['osc']
                        if newoscval != oldoscval:
                            oldoscval = newoscval
                            t += 35 # approx half cycle @ 14.218MHz
                else:
                    newvals = self.inputmap.decode(ioval)

                if not faketime:
                    t = tstamp
                    
                vcd.updatesignals(newvals, t=t)

                if decodevid:
                    # decodevid mode should only return video & refresh addresses
                    # so only thing to do is combine the row and col.
                    # Requires that the memory mode is known (self.samreg.m)
                    # and that the mpu rate is slow
                    if rowflag:
                        vidaddr = vcd['z'].curval
                    else:
                        mm = self.samreg.m
                        mask = (0x3f, 0x7f, 0xff, 0xff)[mm]
                        shift = (6, 7, 8, 8)[mm]
                        vidaddr = ((vidaddr & mask) |
                                  ((vcd['z'].curval & mask) << shift))
                        if  vcd['cas'].curval == 0:
                            # zip file write requires binary data
                            # so we have this bodge to deal with it
                            try:
                                opfile.write(b'v%04x ' % vidaddr)
                            except TypeError:
                                opfile.write('v{:04x} '.format(vidaddr))
                        else:
                            try:
                                opfile.write(b'r%04x ' % vidaddr)
                            except TypeError:
                                opfile.write('r{:04x} '.format(vidaddr))
                        
                    rowflag = not rowflag
                    
                else:
                    # full sampling
                    # determine timing ref points
                    # mpu rate must be slow for this to work
                    oscfalling = vcd['osc'].falling()
                    if tref >= 0:
                        if oscfalling:
                            tref = (tref + 1) & 15
                            vcd.updatesignal('tref', '{:X}'.format(tref), t=t)
                    else:
                        if vcd['q'].rising():
                            tref = 3
                        elif vcd['e'].rising():
                            tref = 7
                        elif vcd['q'].falling():
                            tref = 11
                        elif vcd['e'].falling():
                            tref = 15
                        if tref >= 0:
                            vcd.updatesignal('tref', '{:X}'.format(tref), t=t)
                            
                    # check for write to sam and update register state
                    if vcd['e'].curval:
                        addr = vcd['addr'].curval
                        rw = vcd['rw'].curval
                        if (rw == 0) and (addr >= 0xffc0) and (addr <= 0xffdf):
                            regbit = (addr & 0x1e) >> 1
                            if (addr & 1) == 1:
                                self.samreg.setbit(regbit)
                            else:
                                self.samreg.clrbit(regbit)
                            vcd.updatesignal('samreg', self.samreg.valstring(), t=t)

                    # decode video and refresh addresses
                    if (tref == 0):
                        casflag = False
                        # get first part of address for static memory mode
                        # it's not clear which signal should actually be used to latch the address
                        if (self.samreg.m == 3):
                            vidaddr = vcd['z'].curval
                            bank = 0
                    elif (tref == 1):
                        # dynamic memory modes latch first part of address on falling ras
                        if (self.samreg.m < 3) and vcd['ras0'].falling():
                            vidaddr = vcd['z'].curval
                            bank = 0
                        # banked memory modes use two ras signals
                        elif (self.samreg.m < 2) and vcd['ras1'].falling():
                            vidaddr = vcd['z'].curval
                            bank = 1
                    elif (tref == 3):
                        # second part of address latched on falling cas
                        if vcd['cas'].falling() and (bank >= 0):
                            casflag = True
                            mm = self.samreg.m
                            mask = (0x3f, 0x7f, 0xff, 0xff)[mm]
                            shift = (6, 7, 8, 8)[mm]
                            vidaddr = ((vidaddr & mask) |
                                      ((vcd['z'].curval & mask) << shift))
                            if bank == 1:
                                vidaddr += (0x1000, 0x4000, 0, 0)[mm]
                            vcd.updatesignal('vidaddr', vidaddr, t=t)
                    elif (tref == 4) and not casflag and (bank >= 0):
                        # refresh cycle detected by absence of cas
                        vcd.updatesignal('refresh', vidaddr, t=t)
                        casflag = True
                        

                    self.tref = tref

            print('Received {} samples'.format(samplecount))
            
            # record final state
            self.vcd = vcd


    def mappedaddrtable(self, ident, addrlist):
        """create a table of port values encoded from a list of rw/addr values"""
        tbl = [ident]
        opvals = {}
        for s in addrlist:
            opvals['rw'] = 0 if s[0].upper() == 'W' else 1
            opvals['addr'] = int(s[1:], 0)
            tbl.append('ddw {}'.format(self.outputmap.encode(opvals)))
        return tbl


    def mpucycles(self, addrlist, **kwargs):
        """run a series of mpu address bus cycles"""

        sampenable = kwargs.get('sampenable', True)
        sampstr = 'smpon' if sampenable else 'smpoff'

        opvals = {'osc': 1, 'addr': 0, 'rw': 0, 'hs': 1, 'da0': 0}
        wval0 = self.outputmap.encode(opvals)

        prog = [
            'trst',
            sampstr,            # enable or disable sampling
            'ldri r10 addrtable',
            'ldri r11 {}'.format(len(addrlist)),
            'bsr mpucycles',
            'end',
        ]
        prog.extend(self.subroutines(['mpucycles']))
        prog.extend(self.mappedaddrtable('addrtable:', addrlist))
        self.runprog(prog, **kwargs)


    def resetsam(self, **kwargs):

        addrlist = ['r 0xffff']     # dummy bus cycle to ensure next cycle is complete
        
        # clear all control register bits
        for i in range(16):
            addrlist.append('w 0x{:4x}'.format(0xffc0 + i*2))
        
        kwargs['sampenable'] = kwargs.get('sampenable', False)
        
        self.mpucycles(addrlist, **kwargs)
        self.tref = -1
    
        
    def vsync(self, **kwargs):
        """reset the video address counter
        
        It does this by raising hs while da0 high
        hs is then pulsed low again as the video address counter appears
          to require the extra hs pulse to properly reset
          otherwise the counter won't increment on da0
        The hs pulses also start a ram refresh sequence
        """
        sampenable = kwargs.get('sampenable', False)
        sampstr = 'smpon' if sampenable else 'smpoff'
        prog = [
            'trst',
            sampstr,                    # enable or disable sampling
            'bsr vsync',
            'end',
        ]
        prog.extend(self.subroutines(['vsync']))
        self.runprog(prog, **kwargs)


    def scanline(self, **kwargs):
        """Run a scanline with bus addresses supplied from a table

        performs additional sampling to return video/refresh addresses
          (even if automatic sampling is disabled)
        can only run slow mpu cycles and must be started at point where e has just fallen
        """
        opinit = self.outputmap.encode({'osc': 1})

        sampmode = kwargs.get('sampmode', 'full').lower()
        sampstr = 'smpon' if (sampmode == 'full') else 'smpoff'
        addrlist = kwargs.get('addrlist', [])
        
        addrlen = len(addrlist)
        if addrlen < 57:
            addrlist.extend(['r 0xffff' for i in range(57-addrlen)])

        prog = [
            'trst',
            sampstr,
            'ldri r1 {}'.format(opinit),    # r1 contains initial signals other than rw & address
            'ldri r10 addrlist',
            'bsr scanline',
            'end'
        ]
        prog.extend(self.subroutines(['scanline']))
        prog.extend(self.mappedaddrtable('addrlist:', addrlist))
        self.runprog(prog, **kwargs)


    def vmodetosetuplist(self, vmode):
        """convert video memory mode to a list of sam register writes
    
        vmode       video memory mode 0-7
        """
        wrlist = []
        for a in (0xffc0, 0xffc2, 0xffc4):
            wrlist.append('w 0x{:04x}'.format(a + (vmode & 1)))
            vmode >>= 1
        return wrlist
    
    def vmodechangetowrite(self, vmode, togbit):
        """return sam register write that changes one bit of memory mode
        
        vmode       video memory mode 0-7
        togbit      bit to toggle 0-2
        """
        addr = 0xffc0 + togbit*2
        mask = 1<<togbit
        newbit = ((vmode & mask) ^ mask) >> togbit
        return 'w 0x{:04x}'.format(addr + newbit)
    
    
    def preparemodechangecode(self, initvmode, vmodetogbit, makevcd, opfile=sys.stdout, **kwargs):
        """Prepare the logic tester and sam to run a mode change test routine
        
        initvmode       initial sam video mode 0-7
        vmodetogbit     mode bit to toggle 0-2
        makevcd         generate vcd files True/False
        opfile          file to write video addresses when makevcd=False
        
        First runs a program to prepare the sam memory mode and video mode
        Then writes a subroutine to run a number of scanlines with a mode change at a specific cycle
        Subroutine performs the following steps:
            1. A sam write to restore the initial video mode (after a previous run)
            2. A vsync sequence to preload the video address counter
            3. A variable number of lines with nvma cycles (count in r21)
            4. One line running mpu addresses cycles pointed to by r20
            5. Another variable number of lines with nvma cycles (count in r22)
    
        The subroutine should be called from a short program that sets up r20, r21 & r22
        
        Each scanline runs for 57 sam cycles starting with hsync
        Requires sam in state where e has just fallen
        """
        
        self._makevcd = makevcd
        self._opfile = opfile
    
        # setup sequence
        # this should include the memory type and video mode
        initlist = ['w 0xffdd']     # set m1
        initlist.extend(self.vmodetosetuplist(initvmode))
        vmodechange = self.vmodechangetowrite(initvmode, vmodetogbit)
    
        print(initlist, vmodechange)
    
        # execute the sam setup instructions immediately.
        # full sampling must be kept enabled while this happens so that the decoder
        # can determine the memory type for correct decoding of memory addresses
        vcdfname = 'vcd/setup.vcd' if makevcd else ''
        self.mpucycles(initlist, vcdfilename=vcdfname, **kwargs)
    
        # determine the sam write required to initialise the bit that will
        # be toggled during the display
        modeinitlist = [initlist[vmodetogbit+1]]
    
        # build a subroutine that simulates a number of scanlines
        # full sampling may now be disabled unless a vcd file is required
        sampstr = 'smpon' if makevcd else 'smpoff'
        flushstr = 'flush'
        trststr = 'trst' if makevcd else '' # hides big gaps caused by comms blocking
        opinit = self.outputmap.encode({'osc': 1, 'hs': 1})
        prog = [
            'org 100',
    
            'doscanlines:',
            
            sampstr,
            
            # run the mode init cycles (to init the video mode)
            'ldri r10 setuptable',
            'ldri r11 {}'.format(len(modeinitlist)),
            'jsr mpucycles',
            
            # reset the sam video address counter
            'jsr vsync',
    
            'ldri r1 {}'.format(opinit),    # r1 contains initial signals other than rw & address
    
            # variable number of scanlines with just nvma addresses
            'ldrr r5 r21',
            'beq skip_lines1',
            'slloop1:',
            'ldri r10 nvmatable',
            'bsr scanline',
            flushstr,
            trststr,
            'decr r5',
            'bne slloop1',
            'skip_lines1:',
    
            # one scanline with list of addresses
            'ldrr r10 r20',
            'bsr scanline',
            flushstr,
            trststr,
    
            # variable number of scanlines with just nvma addresses
            'ldrr r5 r22',
            'beq skip_lines2',
            'slloop2:',
            'ldri r10 nvmatable',
            'bsr scanline',
            flushstr,
            trststr,
            'decr r5',
            'bne slloop2',
            'skip_lines2:',
    
            'rts'
        ]
        
        # add required subroutines
        prog.extend(self.subroutines(['mpucycles', 'vsync', 'scanline'], **kwargs))
        
        # add a table containing the setup cycles
        prog.extend(self.mappedaddrtable('setuptable:', modeinitlist))
        
        # add a table containing a full scanline of nvma cycles
        nvmalist = ['r 0xffff' for i in range(57)]
        prog.extend(self.mappedaddrtable('nvmatable:', nvmalist))
    
        # add a table containing the mode change cycle
        addrlist = [vmodechange]
        addrlist.extend(['r 0xffff' for i in range(56)])
        prog.extend(self.mappedaddrtable('addrtable:', addrlist))
    
        # compile program and get pointer to mode change table
        # (the mode change is moved along the scanline by decreasing the pointer value)
        symbols = {}
        objcode = dlcompile(prog, symbols=symbols)
        self._p_addrtable = symbols['addrtable']

        # send program to logic tester
        self.write_prog(objcode, 100)
    
    
    def runmodechangecode(self, numlines, switchpoint, **kwargs):
        """run the previously prepared mode change test subroutine
        
        params:
        numlines        number of scanlines
        switchpoint     cycle number to change video mode
        """
    
        lineno = switchpoint // 57
        cycleno = switchpoint % 57
        print(lineno, cycleno)
    
        linesbefore = lineno
        linesafter = numlines-lineno-1
        prog = [
            'trst',
            'ldri r20 {}'.format(self._p_addrtable - cycleno*4),
            'ldri r21 {}'.format(linesbefore),
            'ldri r22 {}'.format(linesafter),
            'bsr 100',
            'end'
        ]
        
        if self._makevcd:
            sampmode = 'full'
            vcdfname = 'vcd/pos{}.vcd'.format(switchpoint)
            self.reset_timestamp()
        else:
            sampmode = 'video'
            vcdfname = ''
    
        self.runprog(prog, sampmode=sampmode, vcdfilename=vcdfname, file=self._opfile, **kwargs)
        
        self._opfile.write(b'\n')

    
    def subroutines(self, routinelist, **kwargs):

        code = []
        
        if 'mpucycles' in routinelist:
            # runs a number of mpu cycles, with bus addresses from a table
            # hs remains high and da0 remains low
            # params: r10 = address table, r11 = address count
            opvals = {'osc': 1, 'addr': 0, 'rw': 0, 'hs': 1, 'da0': 0}
            op0 = self.outputmap.encode(opvals)
            code.extend([
                'mpucycles:',
                'ldri r1 {}'.format(op0),
                'mpu_cycles_loop:',
                'ldrx r0 r10',
                'orrr r0 r1',
                'mc_wait_e_high:',
                'ldpr p0 r0',
                'clrbpi p0 19',     # osc low
                'tstbpi p2 16',     # test e
                'beq mc_wait_e_high',
                'mc_wait_e_low:',
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'tstbpi p2 16',     # test e
                'bne mc_wait_e_low',
                'decr r11',
                'bne mpu_cycles_loop',
                'rts'
            ])

        if 'vsync' in routinelist:
            # performs a video counter preload
            opvals = {'osc': 1, 'addr': 0xffff, 'rw': 1, 'hs': 0, 'da0': 1}
            op0 = self.outputmap.encode(opvals)
            code.extend([
                'vsync:',
                'ldpi p0 {}'.format(op0),   # hs=0, da0=1
                'bsr clock16',
                'setbpi p0 23',             # hs=1
                'bsr clock16',
                'clrbpi p0 24',             # da0=0
                'bsr clock16',
                'clrbpi p0 23',             # hs=0
                'bsr clock16',
                'setbpi p0 23',             # hs=1
                'for 5',
                'bsr clock16',              # run enough cycles to consume refreshes
                'next',
                'rts',
                'clock16:',
                'for 32',
                'togbpi p0 19',             # toggle osc
                'next',
                'rts'
            ])

        if 'scanline' in routinelist:
            # simulate a scanline with bus addresses supplied from a table
            # performs additional sampling to return video/refresh addresses
            # (even if automatic sampling is disabled)
            # can only run slow mpu cycles and must be started at point where e has just fallen
            # params: r1 = initial signals, r10 = address table (57 entries)
            vidsampstr = 'smpc'
            d32 = not kwargs.get('d64', False)

            if d32:
                code.extend([
                    'scanline:',
                    # d32 first cycle: hs falls after e falls, then da0 falls
                    'clrbri r1 23',     # hs low (init value)
                    'ldrx r2 r10',      # get next bus state (rw & address)
                    'ldrr r0 r1',       # combine init signals with rw & address
                    'orrr r0 r2',       #   to create output value
                    'ldpr p0 r0',       # osc high
                    'clrbpi p0 19',     # osc low
                    'setbpi p0 19',     # osc high
                    'clrbpi p0 24',     # da0 low (port value)
                    'clrbri r1 24',     # da0 low (init value)
                    'clrbpi p0 19',     # osc low
                    'bsr samplevid_b',
                    'bsr wait_e_falling', # complete first cycle
                ])
            else:
                code.extend([
                    'scanline:',
                    # d64 first cycle: da0 falls shortly after e falls
                    # then hs falls after e rises
                    'ldrx r2 r10',      # get next bus state (rw & address)
                    'ldrr r0 r1',       # combine init signals with rw & address
                    'orrr r0 r2',       #   to create output value
                    'ldpr p0 r0',       # osc high
                    'clrbpi p0 19',     # osc low
                    'setbpi p0 19',     # osc high
                    'clrbpi p0 24',     # da0 low (port value)
                    'clrbri r1 24',     # da0 low (init value)
                    'clrbpi p0 19',     # osc low
                    'bsr samplevid_b',
                    'for 4',
                    'setbpi p0 19',     # osc high
                    'clrbpi p0 19',     # osc low
                    'next',
                    'clrbpi p0 23',     # hs low (port value)
                    'clrbri r1 23',     # hs low (init value)
                    'bsr wait_e_falling', # complete first cycle
                ])
            
            code.extend([
                # hs remains low for another 3 cycles
                'for 3',
                'bsr cycle_no_da0_update_bus',
                'next',
            ])
            
            if d32:
                code.extend([
                    # d32 hs rises just before e rises
                    'ldrx r2 r10',      # get next bus state (rw & address)
                    'ldrr r0 r1',       # combine init signals with rw & address
                    'orrr r0 r2',       #   to create output value
                    'bsr samplevid',
                    'for 3',
                    'setbpi p0 19',     # osc high
                    'clrbpi p0 19',     # osc low
                    'next',
                    'setbpi p0 23',     # hs high (port value)
                    'setbri r1 23',     # hs high (init value)
                    'bsr wait_e_falling', # complete cycle
                ])
            else:
                code.extend([
                    # d64 hs rises just after e rises
                     'ldrx r2 r10',      # get next bus state (rw & address)
                    'ldrr r0 r1',       # combine init signals with rw & address
                    'orrr r0 r2',       #   to create output value
                    'bsr samplevid',
                    'for 3',
                    'setbpi p0 19',     # osc high
                    'clrbpi p0 19',     # osc low
                    'next',
                    'setbpi p0 23',     # hs high (port value)
                    'setbri r1 23',     # hs high (init value)
                    'bsr wait_e_falling', # complete cycle
                ])
            
            code.extend([
                # 9 cycles with da0 unchanged (stays low)
                'for 9',
                'bsr cycle_no_da0_update_bus',
                'next',
                
                # 41 cycles with da0 toggling
                'for 41',
                'bsr cycle_with_da0_update_bus',
                'next',
                
                # 2 cycles with da0 unchanged (stays high)
                'for 2',
                'bsr cycle_no_da0_update_bus',
                'next',
                
                'rts',
                
                # subroutine to sample video address row & column
                # must be called at start of cycle
                'samplevid:',
                'ldpr p0 r0',       # osc high
                'clrbpi p0 19',     # osc low
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                # alternative subroutine
                'samplevid_b:',     
                vidsampstr,         # send sample containing video address row
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                vidsampstr,         # send sample containing video address col
                'rts',
                
                # subroutine to run one cycle, no change to da0
                'cycle_no_da0_update_bus:',
                'ldrx r2 r10',      # get next bus address
                'ldrr r0 r1',       # set up output signals with address
                'orrr r0 r2',       #
                'bsr samplevid',
                'wait_e_falling:',
                'wait_e_high:',
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'tstbpi p2 16',     # test e
                'beq wait_e_high',
                'wait_e_low:',
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'tstbpi p2 16',     # test e
                'bne wait_e_low',
                'rts',
                
                # subroutine to run a cycle with da0 toggle near falling q
                'cycle_with_da0_update_bus:',
                'ldrx r2 r10',      # get next bus address
                'ldrr r0 r1',       # set up output signals with address
                'orrr r0 r2',       #
                'bsr samplevid',
                'wait_e_high_da0:',
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'tstbpi p2 16',     # test e
                'beq wait_e_high_da0',
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'setbpi p0 19',     # osc high
                'togbpi p0 24',     # toggle da0 (port value)
                'togbri r1 24',     # toggle da0 (init value)
                'clrbpi p0 19',     # osc low
                'wait_e_low_da0:',
                'setbpi p0 19',     # osc high
                'clrbpi p0 19',     # osc low
                'tstbpi p2 16',     # test e
                'bne wait_e_low_da0',
                'rts',
            ])
            
        return code
