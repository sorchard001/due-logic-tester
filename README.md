## Due Logic Tester

Software to use an Arduino Due as a logic tester or a slow logic analyzer.

Actually, it's more like a fancy USB to parallel interface, but hey, it's got 24 outputs and 24 inputs. That's got to be worth something.

The software includes Python code to drive the I/O and write the results to a value change dump (vcd) file. The vcd file can then be viewed in a waveform viewer such as GTKWave. Waveforms will gain you the respect of your peers and bring you much success.

### Quick warnings

At the time of writing, it is necessary to make a minor modification to a file in the Arduino Due core library to allow a couple of interrupt handlers to be overridden. Have a look at the prerequisites section below to see what's involved.

The Arduino Due uses 3V3 logic levels. You know the drill: If you need to connect to 5V logic then level conversion will be required. Series resistors may be adequate.

You may think that the fastest way to wreck your Arduino Due is to throw it at the nearest wall/floor/bronze idol of Cthulhu, but an even faster way is to short a signal to an adjacent 5V supply pin. Fun times.

Communication is via the native USB port, not the programming USB port. It says which is which on the back of the board. No it didn't take me weeks to notice.

### Principle of operation

The ARM micro on the Due board has a number of digital GPIO ports. Here we are using Port A for outputs and Port C for inputs. The ports are 32 bits in size, but some of the I/O is already used for functions on the board, or not connected, leaving us with 24 inputs and 24 outputs. (Oh, and are they scattered around the headers in a really irritating way? Yes. Yes they are)

Each port has an associated change detect interrupt. This is used to detect changes on the I/O lines and record the port values to a circular buffer. The buffer can hold 8192 samples. Each sample consists of a port ID (A or C), the port state, and a timestamp. The interrupt latency is approximately 1us from the signal changing to the sample being stored, and it should be possible to sample signals of up to approximately 200KHz.

Commands are received and samples are transmitted via the native USB port. Timestamp transmission can be disabled to increase bandwidth. Communication is in ASCII format which is not very efficient but does make it trivially easy to maintain synchronisation.

The inputs have pullup resistors enabled to prevent endless noisy sampling on the unconnected channels. These resistors are very approximately 100K and shouldn't interfere too much with connected circuits.

Manipulating the I/O interactively can be pretty slow due to the two-way latency of USB (millisecond timescales). To improve performance, a program may be uploaded to a virtual machine language interpreter hosted by the Due. This makes it possible to drive the outputs and react to inputs at a far higher rate (sub ten microsecond timescales).

The virtual machine language is fairly simple: It has a small instruction set supporting simple counters and loops, bit manipulations and tests, subroutines and data tables. The Python code includes a symbolic assembler to make it easy to compile and upload programs to the Due.

### Prerequisites

#### Arduino

* [Arduino Due (or clone)](https://store.arduino.cc/arduino-due)
* [Arduino IDE](https://www.arduino.cc/en/software)
* The Due core library. This can be installed via the "Boards Manager" in the Arduino IDE.

If you are new to Arduino then this [getting started guide](https://www.arduino.cc/en/Guide/ArduinoDue) is essential reading. Try getting the LED blinker example working first. The Due is not the most beginner friendly of boards.

One problem you will encounter while trying to compile the logic tester firmware are errors complaining about multiple definitions of PIOA_Handler and PIOC_Handler. This is because I have defined my own handlers for these interrupts and they are intended to override the existing ones in the core library. A modification is required to make this possible.

First find the WInterrupts.c file in the *SAM core* library. There will likely be more than one WInterrupts.c file, each in a different folder, but we are only interested in the one for the Due. The location is more than elbow deep in the mire and will be something like the following. Take some rope with you just in case.

On GNU/Linuxes: `~/.arduino15/packages/arduino/hardware/sam/1.6.12/cores/arduino/WInterrupts.c `

On Windows: `%APPDATA%\Arduino15\packages\arduino\hardware\sam\1.6.12\cores\arduino\WInterrupts.c `

On MacOSXes: `~/Library/Arduino15/packages/arduino/hardware/sam/1.6.12/cores/arduino/WInterrupts.c `

Inside the WInterrupts.c file, find the PIOx_Handler definitions and add the following prototypes above them:

```
void PIOA_Handler(void) __attribute__ ((weak));    // allow handler to be overridden
void PIOC_Handler(void) __attribute__ ((weak));    // allow handler to be overridden
```

#### Other required stuff

* [python3](https://www.python.org/)

* [pyserial](https://pypi.org/project/pyserial/)
```
pip install pyserial
```

* [pyvcd](https://pypi.org/project/pyvcd/)
```
pip install pyvcd
```

* A waveform viewer such as [GTKWave](http://gtkwave.sourceforge.net/)

### Documentation

Further information can be found in the `docs` folder.

The `examples` folder contains some demo scripts. I've included the generated vcd files, so that you can see what the output should look like.

### Troubleshooting

Use your skills to locate the root cause of the problem and solve it like the microelectronic systems ninja that you are. I believe in you.

### Motivation

This project was created purely to help gain insight into undocumented behaviour of the 74LS783 Synchronous Address Multiplexor, or SAM for short. This is a 40 pin IC and needed something with a lot of I/O to drive it and collect data.

The ARM micro in the Due is also called SAM. So much confuse. I've called the project folder `74LS783` and put all the test scripts in there.

### Ideas for improvements

aka Yes I am that lazy

* Add binary sample format to further increase bandwidth
* Add commands to allow the I/O directions to be changed
* Add more I/O lines from ports B & D
