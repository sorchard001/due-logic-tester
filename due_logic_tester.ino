/*
 * Arduino Due Logic Tester
 *
 * 24 outputs and 24 inputs available
 * Outputs can be driven via immediate commands or
 *   by running stored programs using a simple virtual machine language
 * Sampling occurs automatically on I/O changes
 * 
 * Serial communication via native USB port
 * 
 */


// error code n transmitted in *En response
#define ERR_OK 0
#define ERR_BAD_CMD 1
#define ERR_BAD_HEX 2
#define ERR_EXP_LF 3
#define ERR_PROG_MEM 4
#define ERR_PROG_END 5
#define ERR_OPCODE 6
#define ERR_OVERRUN 7
#define ERR_STACK 8
#define ERR_FORNEXT 9

// special error code reserved to indicate normal program exit
// used internally i.e. not transmitted
#define ERR_PROG_DONE 255


// virtual machine code opcodes
#define OP_NOP 0
#define OP_TRST 1
#define OP_FLUSH 2
#define OP_SMPOFF 3
#define OP_SMPON 4
#define OP_SMPA 5
#define OP_SMPC 6
#define OP_FOR 7
#define OP_NEXT 8
#define OP_END 9
#define OP_LDRR 10
#define OP_LDRP 11
#define OP_LDRI 12
#define OP_LDPR 13
#define OP_LDPI 14
#define OP_LDRX 15
#define OP_INCR 16
#define OP_DECR 17
#define OP_ORRI 18
#define OP_ORRR 19
#define OP_ANDRI 20
#define OP_ANDRR 21
#define OP_EORRI 22
#define OP_EORRR 23
#define OP_SETBRI 24
#define OP_SETBPI 25
#define OP_CLRBRI 26
#define OP_CLRBPI 27
#define OP_TOGBRI 28
#define OP_TOGBPI 29
#define OP_TSTBRI 30
#define OP_TSTBPI 31
#define OP_BRA 32
#define OP_BEQ 33
#define OP_BNE 34
#define OP_BSR 35
#define OP_JMP 36
#define OP_JEQ 37
#define OP_JNE 38
#define OP_JSR 39
#define OP_RTS 40

// ----------------------------------------
// Port I/O definitions
// Be careful with these because the DUE has a couple of PortA pins wired to PortC pins
// (A28-C29 & A29-C26)

// define port A output bits
// set bits are outputs (24 output bits)
#define PIOA_BITS 0x0fdffcdf

// define port C input bits
// set bits are inputs (24 input bits)
#define PIOC_BITS 0x33eff3fe

// ----------------------------------------
// cycle counter registers used for timestamp
// cycle counter runs at cpu freq of 84MHz
#define DWT_CONTROL (*(volatile uint32_t *)0xE0001000)
#define DWT_CYCCNT (*(volatile uint32_t *)0xE0001004)
#define SCB_DEMCR (*(volatile uint32_t *)0xE000EDFC)

// ----------------------------------------
// Sample buffer and routines

struct sample_struct {
  uint32_t tstamp;
  uint32_t sampval;
};

// sample buffer size must be a power of 2
#define SBUFSIZE 8192
static struct sample_struct sampbuf[SBUFSIZE];
static uint32_t r_ptr = 0;
static uint32_t w_ptr = 0;

static volatile bool overflow = false;
static uint32_t oldtstamp;

// flag to transmit timestamp with samples
static int tx_tstamp = 1;

// write to sample buffer
static inline void writesample(uint32_t sampval)
{
  sampbuf[w_ptr].tstamp = DWT_CYCCNT;
  sampbuf[w_ptr].sampval = sampval;
  w_ptr = (w_ptr+1) & (SBUFSIZE-1);
  if(w_ptr == r_ptr) overflow = true;
}

// transmit sample buffer via serial
static void outputsamples(void)
{
  struct sample_struct samp;
  static char usbbuf[2048+20];
  char *s = usbbuf;

  while(r_ptr != w_ptr) {
    if(!overflow) {
      samp = sampbuf[r_ptr];
      if(tx_tstamp) {
        uint32_t tdelta = samp.tstamp - oldtstamp;
        oldtstamp = samp.tstamp;
        s += sprintf(s, "+%X %X\n", tdelta, samp.sampval);
      } else {
        s += sprintf(s, "-%X\n", samp.sampval);
      }
      if((s-usbbuf) > 2048) {
        SerialUSB.write(usbbuf, s-usbbuf);
        s = usbbuf;
      }
      r_ptr = (r_ptr+1) & (SBUFSIZE-1);
    } else {
      overflow = false;
      r_ptr = w_ptr;
      SerialUSB.print("*E");
      SerialUSB.print(ERR_OVERRUN);
      SerialUSB.print('\n');
    }
  }
  if(s != usbbuf) SerialUSB.write(usbbuf, s-usbbuf);
}


// ----------------------------------------
// virtual machine code routines

// program memory & registers
static uint8_t progbuf[16384];
static uint8_t *prog_ptr;
static uint32_t regs[256];

// general error code used by virtual machine code and command loop
static int errcode;

// fetch 16 bit immediate
static uint32_t prog_get16(void)
{
  uint32_t result =
        (uint32_t)prog_ptr[0]
    + (((uint32_t)prog_ptr[1])<<8);
  prog_ptr += 2;
  return result;
}

// fetch 32 bit immediate
static uint32_t prog_get32(void)
{
  uint32_t result =
        (uint32_t)prog_ptr[0]
    + (((uint32_t)prog_ptr[1])<<8)
    + (((uint32_t)prog_ptr[2])<<16)
    + (((uint32_t)prog_ptr[3])<<24);
  prog_ptr += 4;
  return result;
}

// load 32 bit value from address
static uint32_t prog_get32x(uint32_t bufpos)
{
  uint8_t *p = &progbuf[bufpos];
  uint32_t result =
        (uint32_t)p[0]
    + (((uint32_t)p[1])<<8)
    + (((uint32_t)p[2])<<16)
    + (((uint32_t)p[3])<<24);
  return result;
}

// ----------------------------------------
// virtual machine code subroutine stack

#define STACKSIZE 8
static uint32_t stack[STACKSIZE];
static int stack_idx = STACKSIZE;

static bool prog_push(uint32_t value)
{
  if(stack_idx > 0) {
    stack[--stack_idx] = value;
    return true;
  } else {
    errcode = ERR_STACK;
    return false;
  }
}

static bool prog_pop(uint32_t *val)
{
  if(stack_idx < STACKSIZE) {
    *val = stack[stack_idx++];
    return true;
  } else {
    errcode = ERR_STACK;
    return false;
  }
}

// ----------------------------------------
// virtual machine code for/next stack

#define FORSTACKSIZE 8
struct for_struct {
  uint32_t count;
  uint8_t *ptr;
};

static struct for_struct forstack[FORSTACKSIZE];
static int forstack_idx = FORSTACKSIZE;

static bool prog_forpush(uint32_t count)
{
  if(forstack_idx > 0) {
    forstack[--forstack_idx].count = count;
    forstack[forstack_idx].ptr = prog_ptr;
    return true;
  } else {
    errcode = ERR_FORNEXT;
    return false;
  }
}

static bool prog_fornext(void)
{
  if(forstack_idx < FORSTACKSIZE) {
    if(--forstack[forstack_idx].count) {
      prog_ptr = forstack[forstack_idx].ptr;
    } else {
      forstack_idx++;
    }
    return true;
  } else {
    errcode = ERR_FORNEXT;
    return false;
  }
}


// ----------------------------------------

// run virtual machine code program
static bool run_prog(uint32_t startpos)
{
  uint32_t param32;
  uint32_t z_val = 0;
  uint16_t param16;
  uint8_t opcode, r0, r1, portnum, param8;
  int8_t signed8;
  
  volatile uint32_t *wr_ports[] = {&REG_PIOA_ODSR, &REG_PIOB_ODSR, &REG_PIOC_ODSR, &REG_PIOD_ODSR};
  volatile const uint32_t *rd_ports[] = {&REG_PIOA_PDSR, &REG_PIOB_PDSR, &REG_PIOC_PDSR, &REG_PIOD_PDSR};
  
  prog_ptr = progbuf + startpos;

  do {
    if (prog_ptr >= (progbuf + sizeof(progbuf))) {
      errcode = ERR_PROG_END;
      break;
    }
    
    opcode = *prog_ptr++;
    
    switch(opcode) {
      case OP_NOP:
        break;

      case OP_TRST:
        oldtstamp = DWT_CYCCNT;
        break;

      case OP_FLUSH:
        outputsamples();
        break;

      case OP_SMPOFF:
        NVIC_DisableIRQ(PIOA_IRQn);
        NVIC_DisableIRQ(PIOC_IRQn);
        break;
        
      case OP_SMPON:
        NVIC_EnableIRQ(PIOA_IRQn);
        NVIC_EnableIRQ(PIOC_IRQn);
        break;
        
      case OP_SMPA:
        writesample(REG_PIOA_PDSR | 0x8000000ul);
        break;
        
      case OP_SMPC:
        writesample(REG_PIOC_PDSR & 0x7ffffffful);
        break;
        
      case OP_FOR:
        param16 = prog_get16();
        prog_forpush(param16);
        break;
        
      case OP_NEXT:
        prog_fornext();
        break;

      case OP_END:
        outputsamples();
        SerialUSB.print("*x\n");
        errcode = ERR_PROG_DONE;
        break;
      
      case OP_LDRR:
        r0 = *prog_ptr++;
        r1 = *prog_ptr++;
        z_val = regs[r0] = regs[r1];
        break;
        
      case OP_LDRP:
        r0 = *prog_ptr++;
        portnum = (*prog_ptr++) & 3;
        z_val = regs[r0] = *rd_ports[portnum];
        break;
        
      case OP_LDRI:
        r0 = *prog_ptr++;
        param32 = prog_get32();
        z_val = regs[r0] = param32;
        break;
        
      case OP_LDPR:
        portnum = (*prog_ptr++) & 3;
        r0 = *prog_ptr++;
        z_val = *wr_ports[portnum] = regs[r0];
        break;
        
      case OP_LDPI:
        portnum = (*prog_ptr++) & 3;
        param32 = prog_get32();
        z_val = *wr_ports[portnum] = param32;
        break;
        
      case OP_LDRX:
        r0 = *prog_ptr++;
        r1 = *prog_ptr++;
        param32 = prog_get32x(regs[r1]);
        regs[r1] += 4;
        z_val = regs[r0] = param32;
        break;
        
      case OP_INCR:
        z_val = ++regs[*prog_ptr];
        prog_ptr++;
        break;
        
      case OP_DECR:
        z_val = --regs[*prog_ptr];
        prog_ptr++;
        break;
        
      case OP_ORRI:
        r0 = *prog_ptr++;
        param32 = prog_get32();
        z_val = (regs[r0] |= param32);
        break;
        
      case OP_ORRR:
        r0 = *prog_ptr++;
        r1 = *prog_ptr++;
        z_val = (regs[r0] |= regs[r1]);
        break;
        
      case OP_ANDRI:
        r0 = *prog_ptr++;
        param32 = prog_get32();
        z_val = (regs[r0] &= param32);
        break;
        
      case OP_ANDRR:
        r0 = *prog_ptr++;
        r1 = *prog_ptr++;
        z_val = (regs[r0] &= regs[r1]);
        break;
        
      case OP_EORRI:
        r0 = *prog_ptr++;
        param32 = prog_get32();
        z_val = (regs[r0] ^= param32);
        break;
        
      case OP_EORRR:
        r0 = *prog_ptr++;
        r1 = *prog_ptr++;
        z_val = (regs[r0] ^= regs[r1]);
        break;
        
      case OP_SETBRI:
        r0 = *prog_ptr++;
        param8 = *prog_ptr++;
        regs[r0] |= (1<<param8);
        break;
        
      case OP_SETBPI:
        portnum = (*prog_ptr++) & 3;
        param8 = *prog_ptr++;
        *wr_ports[portnum] = *rd_ports[portnum] | (1<<param8);
        break;
        
      case OP_CLRBRI:
        r0 = *prog_ptr++;
        param8 = *prog_ptr++;
        regs[r0] &= ~(1<<param8);
        break;
        
      case OP_CLRBPI:
        portnum = (*prog_ptr++) & 3;
        param8 = *prog_ptr++;
        *wr_ports[portnum] = *rd_ports[portnum] & ~(1<<param8);
        break;
        
      case OP_TOGBRI:
        r0 = *prog_ptr++;
        param8 = *prog_ptr++;
        regs[r0] ^= (1<<param8);
        break;
        
      case OP_TOGBPI:
        portnum = (*prog_ptr++) & 3;
        param8 = *prog_ptr++;
        *wr_ports[portnum] = *rd_ports[portnum] ^ (1<<param8);
        break;
        
      case OP_TSTBRI:
        r0 = *prog_ptr++;
        param8 = *prog_ptr++;
        z_val = (regs[r0] & (1<<param8));
        break;
        
      case OP_TSTBPI:
        portnum = (*prog_ptr++) & 3;
        param8 = *prog_ptr++;
        z_val = *rd_ports[portnum] & (1<<param8);
        break;
        
      case OP_BRA:
        signed8 = *prog_ptr++;
        prog_ptr += (int32_t)signed8;
        break;
        
      case OP_BEQ:
        signed8 = *prog_ptr++;
        if(!z_val) {
          prog_ptr += (int32_t)signed8;
        }
        break;
        
      case OP_BNE:
        signed8 = *prog_ptr++;
        if(z_val) {
          prog_ptr += (int32_t)signed8;
        }
        break;
        
      case OP_BSR:
        signed8 = *prog_ptr++;
        if(prog_push((uint32_t)prog_ptr)) {
          prog_ptr += (int32_t)signed8;
        }
        break;
        
      case OP_JMP:
        param16 = prog_get16();
        prog_ptr = &progbuf[param16];
        break;
        
      case OP_JEQ:
        param16 = prog_get16();
        if(!z_val) {
          prog_ptr = &progbuf[param16];
        }
        break;
        
      case OP_JNE:
        param16 = prog_get16();
        if(z_val) {
          prog_ptr = &progbuf[param16];
        }
        break;
        
      case OP_JSR:
        param16 = prog_get16();
        if(prog_push((uint32_t)prog_ptr)) {
          prog_ptr = &progbuf[param16];
        }
        break;
        
      case OP_RTS:
        if(prog_pop(&param32)) {
          prog_ptr = (uint8_t *)param32;
        }
        break;
        
      default:
        errcode = ERR_OPCODE;
    }
    
  } while(!errcode);

  if(errcode == ERR_PROG_DONE) {
    errcode = ERR_OK;
  }

  return(errcode == ERR_OK);
}

// ----------------------------------------
// routines that read serial command input

// wait for incoming character while flushing sample buffer
static uint8_t wait_char(void)
{
  while (!SerialUSB.available()) {
    outputsamples();
  }
  return SerialUSB.read();
}

// read one hex digit from serial
static bool get_hex_digit(uint8_t *hexdig)
{
  uint8_t ch = wait_char();
  
  if(isxdigit(ch)) {
    *hexdig = (ch <= '9') ? ch-'0' : toupper(ch) - ('A'-10);
    return true;
  } else {
    errcode = ERR_BAD_HEX;
    return false;
  }
}

// read specified number of hex digits from serial
static bool get_hex_val(uint32_t *hexval, int numdigs)
{
  uint8_t digit;
  *hexval = 0;
  
  for(int i=numdigs; i; i--) {
    if(!get_hex_digit(&digit)) return false;
    *hexval = (*hexval << 4) + digit;
  }
  
  return true;
}

// receive a program file from serial and write to virtual memory
static bool cmd_prog(void)
{
  uint32_t prog_start, prog_size, prog_byte;
  
  if(!get_hex_val(&prog_start, 4)) return false;
  if(!get_hex_val(&prog_size, 4)) return false;

  if ((prog_start >= sizeof(progbuf))
    || ((prog_start + prog_size) > sizeof(progbuf))) {
    errcode = ERR_PROG_MEM;
    return false;
  }

  for(uint32_t i = 0; i < prog_size; i++) {
    if(!get_hex_val(&prog_byte, 2)) return false;
    progbuf[prog_start + i] = prog_byte;       
  }
  
  return true;  
}


// ----------------------------------------
// Arduino code entry point

void setup() {

  SerialUSB.begin(115200);      // nb. baud rate ignored for native USB
  while(!SerialUSB){};          // wait for connection

  SCB_DEMCR |= 0x1000000;  // enable cycle counter
  oldtstamp = DWT_CYCCNT;
  DWT_CONTROL |= 1;

  // outputs
  pmc_enable_periph_clk(ID_PIOA);
  REG_PIOA_PER = PIOA_BITS;   // controlled by PIO
  REG_PIOA_OER = PIOA_BITS;   // enable output
  REG_PIOA_PUDR = PIOA_BITS;  // disable pullups
  REG_PIOA_IER = PIOA_BITS;   // irq on change
  NVIC_SetPriority(PIOA_IRQn, 0);
  NVIC_EnableIRQ(PIOA_IRQn);

  // inputs
  pmc_enable_periph_clk(ID_PIOC);
  REG_PIOC_PER = PIOC_BITS;   // controlled by PIO
  REG_PIOC_ODR = PIOC_BITS;   // disable output
  REG_PIOC_PUER = PIOC_BITS;  // enable pullups
  REG_PIOC_IER = PIOC_BITS;   // irq on change
  NVIC_SetPriority(PIOC_IRQn, 0);
  NVIC_EnableIRQ(PIOC_IRQn);

  pinMode(LED_BUILTIN, OUTPUT);
  
  SerialUSB.print("\n*DUE Logic Tester\n");

  progbuf[0] = OP_END;
}

// ----------------------------------------
// main command loop

void loop() {
  uint8_t ch;
  uint32_t param;
  bool lf_expected;
  bool prog_error;

  errcode = ERR_OK;
  prog_error = false;
  lf_expected = true;

  ch = wait_char();
  switch(ch) {
    case 10:
    case 13:
      SerialUSB.print(">\n");
      lf_expected = false;
      break;

    case 's':
      // comms synchronisation
      // sxxxxxxxx\n
      // returns *sxxxxxxxx\n
      if(get_hex_val(&param, 8)) {
        char s[20];
        sprintf(s, "*s%08X\n", param);
        SerialUSB.print(s);
      }
      break;

    case 'w':
      // write output value xxxxxxxx
      // wxxxxxxxx\n
      if(get_hex_val(&param, 8)) {
        REG_PIOA_ODSR = param & PIOA_BITS;
        SerialUSB.print("#\n");
      }
      break;
    
    case 'p':
      // send program address xxxx size yyyy
      // pxxxxyyyy\n
      if(cmd_prog()) {
        SerialUSB.print("#\n");
      }
      break;
        
    case 'r':
      // run program from address xxxx
      // rxxxx\n
      if(get_hex_val(&param, 4)) {
        prog_error = !run_prog(param);
      }
      break;

    case 't':
      // transmit timestamp x = 0 off, x != 0 on, 
      // tx\n
      if(get_hex_val(&param, 1)) {
        tx_tstamp = param;  
        SerialUSB.print("#\n");
      }
      break;

    case 'x':
      // request end of sample marker
      SerialUSB.print("*x\n");
      break;
      
    case 'z':
      // zero timestamp
      // z\n
      oldtstamp = DWT_CYCCNT;
      SerialUSB.print("#\n");
      break;

    default:
      while(wait_char() != 10) ;
      errcode = ERR_BAD_CMD;
      lf_expected = false;
      break;
  }

  if(lf_expected) {
    if(wait_char() != 10) {
      if(errcode == ERR_OK) errcode = ERR_EXP_LF;       
    }
  }

  if(errcode != ERR_OK) {
    SerialUSB.print("*E");
    SerialUSB.print(errcode);
    if(prog_error) {
      SerialUSB.print(' ');
      SerialUSB.print((uint32_t)(prog_ptr-progbuf), HEX);
    }
    SerialUSB.print('\n');
  }

}


// IO port A interrupt handler
// When change detected, adds current port state to circular buffer
// Top bit is set to identify value from port A
// May require mod to WInterrupts.c : add the following prototye before the function definition:
// void PIOA_Handler(void) __attribute__ ((weak));
void PIOA_Handler(void)
{
  uint32_t isr = REG_PIOA_ISR;
  writesample(REG_PIOA_PDSR | 0x80000000ul);
}

// IO port C interrupt handler
// When change detected, adds current port state to circular buffer
// Top bit is cleared to identify value from port C
// May require mod to WInterrupts.c : add the following prototye before the function definition:
// void PIOC_Handler(void) __attribute__ ((weak));
void PIOC_Handler(void)
{
  uint32_t isr = REG_PIOC_ISR;
  writesample(REG_PIOC_PDSR & 0x7ffffffful);
}
