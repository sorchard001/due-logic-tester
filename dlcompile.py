#!/usr/bin/python3

import sys
import struct


class DLCompError(Exception):
    def __init__(self, mainmsg='Compilation error', *args):
        self.message = mainmsg
        self.addinfo(*args)

    def __str__(self):
        if self.lineno >= 0:
            return '{} in line {}:\n  ... {}\n'.format(
                self.message, self.lineno+1, self.linetext)
        else:
            return self.message

    def addinfo(self, lineno=-1, linetext='', msg=''):
        self.lineno = lineno
        self.linetext = linetext
        if len(msg) > 0:
            self.message = msg


class DLCompUnknownMnemonic(DLCompError):
    def __init__(self, *args):
        super().__init__('Unrecognised mnemonic', *args)

class DLCompMissingOperand(DLCompError):
    def __init__(self, *args):
        super().__init__('Missing operand', *args)

class DLNoOperandsExpected(DLCompError):
    def __init__(self, *args):
        super().__init__('No operands expected', *args)

class DLCompBadReg(DLCompError):
    def __init__(self, *args):
        super().__init__('Bad register specification (expected r0-r255)', *args)

class DLCompBadPort(DLCompError):
    def __init__(self, *args):
        super().__init__('Bad port specification (expected p0-p3)', *args)

class DLCompUndefined(DLCompError):
    def __init__(self, *args):
        super().__init__('Symbol not defined', *args)

class DLCompDuplicate(DLCompError):
    def __init__(self, *args):
        super().__init__('Symbol already defined', *args)

class DLCompBadOff8(DLCompError):
    def __init__(self, *args):
        super().__init__('Offset too large for 8 bits', *args)



def _pack16(val):
    """convert value into little-endian list of 2 bytes"""
    return [a for a in struct.pack('<H', val)]

def _pack32(val):
    """convert value into little-endian list of 4 bytes"""
    return [a for a in struct.pack('<L', val)]

def _regnum(reg):
    """check reg is valid spec rn and return n"""
    if reg[:1].upper() != 'R':
        raise DLCompBadReg
    n = int(reg[1:])
    if (n<0) or (n>255):
        raise DLCompBadReg
    return n

def _portnum(port):
    """check port is valid spec pn and return n"""
    if port[:1].upper() != 'P':
        raise DLCompBadPort
    n = int(port[1:])
    if (n<0) or (n>3):
        raise DLCompBadPort
    return n

def _getval(token):
    """return value of token as either numeric or symbol lookup"""
    if token[0].isalpha():
        if _pass1:
            val = _pc
        else:
            try:
                val = _symbols[token]
            except KeyError as e:
                raise DLCompUndefined from e
    else:
        val = int(token, 0)
    return val


def _fn_inherent(tokens):
    """check no operands have been specified for inherent opcode"""
    if len(tokens) > 1:
        raise DLNoOperandsExpected
    return []

def _fn_imm8(tokens):
    """return post byte as list for 8 bit immediate"""
    if len(tokens) <= 1:
        raise DLCompMissingOperand
    return [_getval(tokens[1])]

def _fn_off8(tokens):
    """return post byte as list for 8 bit pc offset"""
    if len(tokens) <= 1:
        raise DLCompMissingOperand
    val = _getval(tokens[1])
    off8 = val - _pc - 2
    if (off8 < -128) or (off8 > 127):
        raise DLCompBadOff8
    return [off8 & 0xff]

def _fn_imm16(tokens):
    """return post bytes as list for 16 bit immediate"""
    if len(tokens) <= 1:
        raise DLCompMissingOperand
    i = _getval(tokens[1])
    return _pack16(i)

def _fn_imm32(tokens):
    """return post bytes as list for 32 bit immediate"""
    if len(tokens) <= 1:
        raise DLCompMissingOperand
    i = _getval(tokens[1])
    return _pack32(i)

def _fn_reg(tokens):
    """return post byte as list for register operand"""
    if len(tokens) <= 1:
        raise DLCompMissingOperand
    r = tokens[1]
    return [_regnum(r)]

def _fn_reg_reg(tokens):
    """return post bytes as list for two register operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    r1 = tokens[1]
    r2 = tokens[2]
    return [_regnum(r1), _regnum(r2)]

def _fn_reg_imm8(tokens):
    """return post bytes as list for register and 8 bit immediate operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    r = tokens[1]
    result = [_regnum(r)]
    i = _getval(tokens[2])
    result.append(i)
    return result

def _fn_reg_imm32(tokens):
    """return post bytes as list for register and 16 bit immediate operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    r = tokens[1]
    result = [_regnum(r)]
    i = _getval(tokens[2])
    result.extend(_pack32(i))
    return result

def _fn_port_imm8(tokens):
    """return post bytes as list for port and 8 bit immediate operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    p = tokens[1]
    result = [_portnum(p)]
    i = _getval(tokens[2])
    result.append(i)
    return result

def _fn_port_imm32(tokens):
    """return post bytes as list for port and 32 bit immediate operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    p = tokens[1]
    result = [_portnum(p)]
    i = _getval(tokens[2])
    result.extend(_pack32(i))
    return result

def _fn_port_reg(tokens):
    """return post bytes as list for port and register operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    p = tokens[1]
    r = tokens[2]
    return [_portnum(p), _regnum(r)]

def _fn_reg_port(tokens):
    """return post bytes as list for register and port operands"""
    if len(tokens) <= 2:
        raise DLCompMissingOperand
    r = tokens[1]
    p = tokens[2]
    return [_regnum(r), _portnum(p)]

def _fn_org(tokens):
    """change program counter"""
    global _pc
    if len(tokens) <= 1:
        raise DLCompMissingOperand
    _pc = _getval(tokens[1])
    return []


_opdefs = (
    ('nop',     _fn_inherent),
    ('trst',    _fn_inherent),
    ('flush',   _fn_inherent),
    ('smpoff',  _fn_inherent),
    ('smpon',   _fn_inherent),
    ('smpa',    _fn_inherent),
    ('smpc',    _fn_inherent),
    ('for',     _fn_imm16),
    ('next',    _fn_inherent),
    ('end',     _fn_inherent),

    ('ldrr',    _fn_reg_reg),
    ('ldrp',    _fn_reg_port),
    ('ldri',    _fn_reg_imm32),
    ('ldpr',    _fn_port_reg),
    ('ldpi',    _fn_port_imm32),
    ('ldrx',    _fn_reg_reg),

    ('incr',    _fn_reg),
    ('decr',    _fn_reg),

    ('orri',    _fn_reg_imm32),
    ('orrr',    _fn_reg_reg),
    ('andri',   _fn_reg_imm32),
    ('andrr',   _fn_reg_reg),
    ('eorri',   _fn_reg_imm32),
    ('eorrr',   _fn_reg_reg),

    ('setbri',  _fn_reg_imm8),
    ('setbpi',  _fn_port_imm8),
    ('clrbri',  _fn_reg_imm8),
    ('clrbpi',  _fn_port_imm8),
    ('togbri',  _fn_reg_imm8),
    ('togbpi',  _fn_port_imm8),
    ('tstbri',  _fn_reg_imm8),
    ('tstbpi',  _fn_port_imm8),

    ('bra',     _fn_off8),
    ('beq',     _fn_off8),
    ('bne',     _fn_off8),
    ('bsr',     _fn_off8),
    ('jmp',     _fn_imm16),
    ('jeq',     _fn_imm16),
    ('jne',     _fn_imm16),
    ('jsr',     _fn_imm16),
    ('rts',     _fn_inherent)
)

_dirdefs = (
    ('org',     _fn_org),
    ('ddw',     _fn_imm32)
)


# build a dictionary out of the opcode and directive definitions
_opdict = {mnemonic: (opcode, fn) for opcode, (mnemonic, fn) in enumerate(_opdefs)}
_opdict.update({mnemonic: (-1, fn) for mnemonic, fn in _dirdefs})



def dlcompile(lines, **kwargs):
    global _pc
    global _symbols
    global _pass1

    _symbols = kwargs.get('symbols', {})
    showcode = kwargs.get('showcode', False)
    objcode = []

    for _pass1 in (True, False):

        _pc = 0

        for lineno, ln in enumerate(lines):

            s = ln.split()

            if len(s) > 0:
                if s[0][:1] == ';':
                    continue
                elif s[0][-1:] == ':':
                    symb = s[0][:-1]
                    if _pass1 and (symb in _symbols):
                        raise DLCompDuplicate(lineno, ln)
                    _symbols[symb] = _pc
                    del s[0]
                    if showcode and not _pass1:
                        print('{:04X}  {:12} {}'.format(_pc, '', symb))

            if len(s) > 0:
                try:
                    opcode, opfn = _opdict[s[0].lower()]
                    opcodes = [opcode] if opcode >=0 else []
                    opcodes.extend(opfn(s))
                except KeyError as e:
                    raise DLCompUnknownMnemonic(lineno, ln) from e
                except DLCompError as e:
                    e.addinfo(lineno, ln)
                    raise e
                except (ValueError, struct.error) as e:
                    # catches problems with arguments
                    raise DLCompError(str(e), lineno, ln) from e

                if not _pass1:
                    objcode.extend(opcodes)
                    if showcode:
                        opshex = ' '.join('{:02X}'.format(x) for x in opcodes)
                        print('{:04X}  {:20} {}'.format(_pc, opshex, ' '.join(s)))

                _pc += len(opcodes)

    return  objcode


if __name__ == '__main__':

    # output a list of mnemonics and opcodes as #define statements
    # for use in Arduino code
    for mnemonic, (opcode, fn) in _opdict.items():
        if opcode >= 0:
            print('#define OP_{} {}'.format(mnemonic.upper(), opcode))
