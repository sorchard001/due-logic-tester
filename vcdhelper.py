#!/usr/bin/python3

"""VCD (Value Change Dump) helper classes

Requires pyVCD
pip3 install pyvcd
"""

import datetime

from vcd import VCDWriter


class PinMap(dict):
    """Dictionary class that associates I/O port bits with signal names
    
    Each dictionary entry is a list of one or more port bit numbers
    The first bit in each signal list maps to the lsb of the signal
    Bit numbers start from 0
    """
    def __init__(self, *args, **kwargs):
        self.update(*args, **kwargs)

    def decode(self, wordval):
        """Decode a port value into a dictionary of signal values

        Multiple signals may reference the same port bits
        """
        result = {}
        for name, pins in self.items():
            val = 0
            for shift, pin in enumerate(pins):
                val = val | ((wordval & (1<<pin)) >> (pin - shift))
            result[name] = val
        return result

    def encode(self, vals):
        """Encode a dictionary of signal values into a port value
        
        If a port bit is referenced multiple times then its value will be the logical OR
        of the contributing signal bits
        """
        result = 0
        for name, val in vals.items():
            for shift, pin in enumerate(self[name]):
                result = result | ((val & (1<<shift)) << (pin - shift))
        return result


class Signal:
    """Signal class provides methods for detecting signal edges
    
    Only changes that happened during the last update are reported
    The change is only reported once
    """
    def __init__(self, **kwargs):
        self.curval = None
        self.oldval = None
        self.category = kwargs.get('category', '')

    def setval(self, newval, **kwargs):
        self.oldval = self.curval
        self.curval = newval

    def rising(self):
        c = ((self.oldval != None) and (self.curval > self.oldval))
        if c:
            self.oldval = self.curval
        return c

    def falling(self):
        c = ((self.oldval != None) and (self.curval < self.oldval))
        if c:
            self.oldval = self.curval
        return c

#    def changed(self):
#        c = (self.curval != self.oldval)
#        if c:
#           self.oldval = self.curval
#        return c


class SignalDict(dict):

    def addsignal(self, name, **kwargs):
        self[name] = Signal(**kwargs)
            
    def addpinmap(self, pinmap, **kwargs):
        for name, pins in pinmap.items():
            self.addsignal(name, **kwargs)
            
    def updatesignal(self, name, newval, **kwargs):
        self[name].setval(newval)

    def updatesignals(self, newvals, **kwargs):
        for name, newval in newvals.items():
            self[name].setval(newval)


class VCDSignalWriter(VCDWriter, SignalDict):

    def __init__(self, filename, **kwargs):
        self.file = open(filename, 'w')
        if not 'date' in kwargs:
            kwargs['date'] = str(datetime.datetime.now())
        super().__init__(self.file, **kwargs)

    def close(self, *args, **kwargs):
        super().close(*args, **kwargs)
        try:
            self.file.close()
        except:
            print('Failed on close vcd file')

    def addsignal(self, name, **kwargs):
        super().addsignal(name, **kwargs)
        s = self[name]
        s.vcdvar = self.register_var(s.category, name, kwargs['type'], kwargs['size'])

    def addpinmap(self, pinmap, **kwargs):
        for name, pins in pinmap.items():
            kwargs['type'] = 'wire'
            kwargs['size'] = len(pins)
            self.addsignal(name, **kwargs)

    def updatesignal(self, name, newval, **kwargs):
        tstamp = kwargs.get('t', 0)
        s = self[name]
        s.setval(newval)
        if s.oldval != newval:
            self.change(s.vcdvar, tstamp, newval)

    def updatesignals(self, newvals, **kwargs):
        tstamp = kwargs.get('t', 0)
        for name, newval in newvals.items():
            s = self[name]
            s.setval(newval)
            if s.oldval != newval:
                self.change(s.vcdvar, tstamp, newval)



if __name__ == '__main__':

    print('VCD Helper Test\n')

    outputmap = PinMap(
        osc = [1],
        addr = [2,3,4,5],
        hs = [6]
    )

    inputmap = PinMap(
        vclk = [0],
        e = [1],
        q = [2]
    )

    with VCDSignalWriter('vcdhelper_test.vcd', timescale='1 us') as vdump:

        vdump.addsignal('descrip', category='comments', type='string', size=10)
        vdump.updatesignal('descrip', 'Test', t=0)

        vdump.addsignal('number', category='comments', type='wire', size=16)

        vdump.addpinmap(outputmap, category='outputs')
        vdump.addpinmap(inputmap, category='inputs')

        for tstamp, value in enumerate(range(64)):

            opvals = outputmap.decode(value)
            vdump.updatesignals(opvals, t=tstamp)

            ipvals = inputmap.decode(value*13)
            vdump.updatesignals(ipvals, t=tstamp)

            vdump.updatesignal('number', tstamp*11, t=tstamp)

        for name, sig in vdump.items():
            print(name, sig.category, sig.curval)
