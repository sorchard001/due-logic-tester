#!/usr/bin/python3

"""Arduino Due Logic Tester communication interface

Provides functions for all communication

requires pyserial
pip install pyserial
"""

import serial
import sys
import time
import struct
import random


# Define default serial port (e.g. Linux)
#DEFAULTSERPORT = '/dev/ttyACM0'

# Define default serial port (e.g. Windows)
DEFAULTSERPORT = 'COM30'


class DLError(Exception):
    def __init__(self, message=''):
        self.message = message
    def __str__(self):
        return self.message

class DLSyncError(DLError):
    def __init__(self):
        self.message = 'Failed to acquire sync'
        #super().__init__('Failed to acquire sync')

class DLDecodeError(DLError):
    def __init__(self, val):
        self.message = 'Unrecognised response: {0}'.format(val)

class DLTesterError(DLError):
    def __init__(self, ln):
        s = ln.decode().split()
        errcode = s.pop(0)[2:]
        errmsg, errdetail = {
            '0': ('No error', ''),
            '1': ('Unrecognised immediate command', ''),
            '2': ('Bad hexadecimal value', ''),
            '3': ('Expected linefeed', ''),
            '4': ('Program does not fit in memory', ''),
            '5': ('Program counter past end of memory', 'pc ='),
            '6': ('Unrecognised opcode', 'pc ='),
            '7': ('Sample buffer overflow', ''),
            '8': ('Stack overflow/underflow', 'pc ='),
            '9': ('For/next error', 'pc =')
        }.get(errcode, ('unknown error code', ''))
        self.message = 'Received error code {}: {}'.format(errcode, errmsg)
        if len(s) > 0:
            self.message += ' ({} {})'.format(errdetail, ''.join(s))


class DLSerial(serial.Serial):

    def __init__(self, serport=DEFAULTSERPORT):
        super().__init__(serport, 115200, timeout=1)
        self.oldtstamp = 0
        self.zero_time = time.perf_counter()
        self.buflines = []
        self.rxbuf = bytearray()


    def __readline(self):
        """Much faster version of serial.readline"""
        i = self.rxbuf.find(b'\n')
        if i >= 0:
            r = self.rxbuf[:i+1]
            del self.rxbuf[:i+1]
            return r
        while True:
            i = max(1, min(2048, self.in_waiting))
            data = self.read(i)
            if len(data) < 1:
                return b''
            i = data.find(b'\n')
            if i >= 0:
                r = self.rxbuf + data[:i+1]
                self.rxbuf[0:] = data[i+1:]   # convert bytes to bytearray
                return r
            else:
                self.rxbuf.extend(data)


    def __check_response(self):
        """Wait for response from logic tester
        
        Waits for a line beginning with '#'
        Any samples received are added to buffer
        """
        while True:
            ln = self.__readline()
            if ln[:1] == b'#':
                break
            if(ln[:1] in b'+-') or (ln[:2] == b'*x'):
                self.buflines.append(ln)
            elif (ln[:2] == b'*E'):     # error code received
                raise DLTesterError(ln)
            else:
                raise DLDecodeError(ln)


    def get_sync(self):
        """Ensure communication with tester is synchronised

        Send sync command with pseudo-random payload and wait for matching response
        Any samples received are discarded
        """
        syncstr = b'*s%08X\n' % (random.randint(0, 0xffffffff))
        self.write(syncstr[1:])
        while True:
            ln = self.__readline()
            if ln[:10] == syncstr[:10]:
                break
            if (ln[:2] == b'*E'):       # error code received
                raise DLTesterError(ln)
            if len(ln) == 0:            # timed out
                raise DLSyncError


    def reset_timestamp(self):
        """Zero the timestamp both locally and in the logic tester"""
        self.write(b'z\n')
        self.oldtstamp = 0
        self.zero_time = time.perf_counter()
        self.__check_response()

        
    def enable_timestamp(self, enable):
        """Enable/disable timestamp transmission

        enable specified as 0 or 1
        """
        bena = b'1' if enable else b'0'
        self.write(b''.join((b't', bena, b'\n')))
        self.__check_response()


    def write_outp(self, opval):
        """Immediately write a value to the output port"""
        self.write(b'w%08X\n' % opval)
        self.__check_response()


    def end_of_samples(self):
        """Requests an end of sample marker
        
        This allows get_samples() to complete early
        instead of timing out
        """
        self.write(b'x\n')


    def write_prog(self, program, startaddr=0):
        """Upload a program to the logic tester memory
        
        program is a list of opcodes & operands
        program length is 16384 max
        """
        self.write(
            b''.join((
                b'p%04X%04X' % (startaddr, len(program)),
                b''.join(b'%02X' % b for b in program),
                b'\n')))
        self.__check_response()


    def run_prog(self, startaddr=0):
        """Run previously uploaded program at specified address"""
        self.write(b'r%04X\n' % startaddr)


    def get_samples(self):
        """Generator that yields received samples
        
        Yields the tuple (tstamp, id, ioval)
        tstamp is the timestamp in nanoseconds
        id is 1 for port A (outputs), 0 for port C (inputs)
        ioval is the port value
        """
        while True:
            if len(self.buflines) > 0:
                ln = self.buflines.pop(0)
            else:
                ln = self.__readline()
            if (ln[:1] == b'-'):    # sample without timestamp
                s = ln[1:]
                if len(s) > 0:
                    tstamp = round((time.perf_counter() - self.zero_time)*1e9)  # nanoseconds
                    ioval = int(s, 16)
                    id = (ioval >> 31) & 1
                    yield tstamp, id, ioval
                else:
                    raise DLDecodeError(ln)
            elif (ln[:1] == b'+'):     # sample with timestamp
                s = ln.split()
                if len(s) == 2:
                    tdelta = round(int(s[0], 16) * (1000/84))   # nanoseconds
                    tstamp = self.oldtstamp + tdelta
                    self.oldtstamp = tstamp
                    ioval = int(s[1], 16)
                    id = (ioval >> 31) & 1
                    yield tstamp, id, ioval
                else:
                    raise DLDecodeError(ln)
            elif (ln[:2] == b'*x') or (len(ln) == 0):
                # program reached 'end' statement or no more samples
                break
            elif (ln[:2] == b'*E'):         # error code received
                raise DLTesterError(ln)
            else:
                raise DLDecodeError(ln)


if __name__ == '__main__':

    print('DLSerial Connection Test')

    with DLSerial() as dl:
        print('Waiting for sync...')
        dl.get_sync()
        print('Sync acquired. Test successful')

